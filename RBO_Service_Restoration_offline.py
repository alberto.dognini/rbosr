###############################################################################
# Rule-based optimized alfgorithm for Service Restoration
#
# @author Alberto Dognini <adognini@eonerc.rwth-aachen.de>
# @author Abhinav Sadu <asadu@eonerc.rwth-aachen.de>
# @copyright 2019,  Institute for Automation of Complex Power Systems, RWTH Aachen University
# @license GNU General Public License (version 3)
#
# RBOSR
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import sys
import os
import time
import xlsxwriter
import pandas as pd
import networkx as net
import math
import cmath
import numpy as np
import psycopg2
from datetime import datetime, timedelta
from DSSE_Functions_FLISR import connDB, DSSEVRI1

#CODE AT THE END OF THE FILE; AFTER FUNCTIONS AND CLASSES

#FLISR algorithm function, iterate the research of restoration path and check multiple faults/modifications of the grid:
def flisr_algorithm(conn_SE, conn_txt, is_fault_detected, is_FLISR_in_operation, general, u1, u2, u3, u4):
    is_fault_identified = 0

    ###########################################################################################################################################
    ####################################### Import data from the database for the FLISR algorithm #############################################
    update_global_FLISR_start_flag(conn_txt)
    ###########################################################################################################################################
    ####################################### Import data from the database for the FLISR algorithm #############################################
    switches_updated = fetch_switches_flisr_table(conn_txt)
    lines_updated = fetch_lines_flisr_table(conn_txt)
    loads_updated = fetch_loads_flisr_table(conn_txt)
    nodes_updated = fetch_nodes_flisr_table(conn_txt)
    line_type, last_fetch_time_ltype = fetch_lines_type_table(conn_txt)

    #############################################################################
    ##################    Check for tripped switches   ###########################
    switch_updated, is_fault_identified = check_fault(conn_txt, is_fault_identified)
    #    print('updated tripped switches',switch_updated)
    #    print('update flag "FLISR_FAULT_IDENTIFIED"')
    update_fault_identification_flag(is_fault_identified, conn_txt)
    #    print('Initiating FLISR: Update flag _FLISR_IN_OPERATION')
    is_FLISR_in_operation = 1
    update_FLISR_operation_flag(is_FLISR_in_operation, conn_txt)
    ##################################################################################
    #########################    run the FLISR       ##################################

    subgrid = creation_grid(switches_updated, lines_updated)

    [set_SS, alive_loads, grid_lines, grid_switches, grid_loads, grid_nodes, tripped_nodes, all_possible_loads,
     alive_grid] = flisr_loads(subgrid, nodes_updated, loads_updated, lines_updated, switches_updated, line_type)
    if all_possible_loads == 0:
        [to_be_closed, final_path, load, subst, all_possible_loads] = flisr_dijkstra(conn_SE, set_SS, alive_loads,
                                                                                     grid_lines, grid_switches,
                                                                                     grid_loads, grid_nodes, line_type,
                                                                                     loads_updated, nodes_updated,
                                                                                     alive_grid, general, u1, u2, u3,
                                                                                     u4)
    else:
        to_be_closed = 0
        return to_be_closed, subgrid, set_SS, switches_updated, lines_updated, loads_updated, nodes_updated, all_possible_loads

    ###############################################################################
    ############### check if additional faults happened ########################
    switch_updated1, is_fault_identified = check_fault(conn_txt, is_fault_identified)  #### restart the fault check
    while not set(switch_updated1) == set(tripped_nodes):  ## verify if the tripped nodes are the same
        print('Determining the additional faults : Restarting check_fault()')
        print('updated tripped switches', switch_updated1)
        print('update flag "FLISR_FAULT_IDENTIFIED"')
        update_fault_identification_flag(is_fault_identified, conn_txt)

        ################ Restarting the FLISR  #######################################
        print(' Re-initiating FLISR')
        ################################## Import data from the database for the FLISR algorithm ###################################################
        switches_updated = fetch_switches_flisr_table(conn_txt)
        lines_updated = fetch_lines_flisr_table(conn_txt)
        loads_updated = fetch_loads_flisr_table(conn_txt)
        nodes_updated = fetch_nodes_flisr_table(conn_txt)

        ################# Running FLISR again ###############################################
        is_FLISR_in_operation = 1
        subgrid = creation_grid(switches_updated, lines_updated)

        [set_SS, alive_loads, grid_lines, grid_switches, grid_loads, grid_nodes, tripped_nodes, all_possible_loads,
         alive_grid] = flisr_loads(subgrid, nodes_updated, loads_updated, lines_updated, switches_updated, line_type)
        if all_possible_loads == 0:
            [to_be_closed, final_path, load, subst, all_possible_loads] = flisr_dijkstra(conn_SE, set_SS, alive_loads,
                                                                                         grid_lines, grid_switches,
                                                                                         grid_loads, grid_nodes,
                                                                                         line_type, loads_updated,
                                                                                         nodes_updated, alive_grid,
                                                                                         general, u1, u2, u3, u4)
        else:
            to_be_closed = 0
            return to_be_closed, subgrid, set_SS, switches_updated, lines_updated, loads_updated, nodes_updated, all_possible_loads

        switch_updated1 = check_fault(conn_txt,
                                      is_fault_identified)  ### at the end of flisr, always carry out a fault check

    print('No more faults happened during FLISR')
    print('FLISR completed successfully')
    is_FLISR_in_operation = 0
    is_FLISR_failed = 0
    #########################################################################################
    #####################    Final Updation of FLISR completion results of FLISR #########
    if not final_path == []:
        print('the connection between load', load, 'and substation', subst, ' is:', final_path)
        print('close the switches', to_be_closed, 'immediately!!!!')
        #        print('update flag "FLISR_PUBLISHED"')
        update_FLISR_publish_flag(is_FLISR_failed, is_FLISR_in_operation, conn_txt)
        return to_be_closed, subgrid, set_SS, switches_updated, lines_updated, loads_updated, nodes_updated, all_possible_loads
    else:
        to_be_closed = 0
        all_possible_loads == 1
        print('The FLISR is concluded, no more loads can be reconnected with a suitable path')
        return to_be_closed, subgrid, set_SS, switches_updated, lines_updated, loads_updated, nodes_updated, all_possible_loads


###############################################################################
### FUNCTION TO CREATE THE GRID OF CLOSED OR NO SWITCHES; TO BE USED FOR STATE ESTIMATION ###

def creation_grid(switches_updated, lines_updated):
    # building the graph and verifying that the line is operating
    for i, elrow in lines_updated.iterrows():
        if elrow[4] == True:
            grid_lines.add_edge(int(elrow[2]), int(elrow[3]), attr_dict=elrow[0:].to_dict())

    #####additional graph for switches (to check which tripped)#####
    # creating the graph of switches
    for i, elrow in switches_updated.iterrows():
        grid_switches.add_edge(int(elrow[2]), int(elrow[3]), attr_dict=elrow[1:].to_dict())

    subgrid = net.Graph(
        (source, target, attr) for source, target, attr in grid_lines.edges_iter(data=True))  # copy of total graph
    for (p, x, d) in grid_switches.edges(data=True):
        if (d['status'] == 0):  # removal of open switch lines
            subgrid.remove_edge(p, x)

    print('edges', subgrid.edges())
    print('FLISR : Step 1 : Creating the real time grid topology')

    return subgrid;


###############################################################################
############## FUNCTION TO IDENTIFY THE LOADS TO BE CONNECTED #################

def flisr_loads(subgrid, nodes_updated, loads_updated, lines_updated, switches_updated, line_type):
    # building the graph and verifying that the line is operating
    for i, elrow in lines_updated.iterrows():
        if elrow[4] == True:
            grid_lines.add_edge(int(elrow[2]), int(elrow[3]), attr_dict=elrow[1:].to_dict())

    # add attribute column containing the line impedance and complex impedance
    net.set_edge_attributes(grid_lines, 'impedance line',
                            1)  # at the beginning, adding a new column attribute with a random value
    net.set_edge_attributes(grid_lines, 'complex impedance', 1)
    net.set_edge_attributes(grid_lines, 'admittance', 1)
    net.set_edge_attributes(grid_lines, 'complex admittance', 1)
    for (a, b, c) in grid_lines.edges(data=True):
        for o, elrow in line_type.iterrows():
            if elrow[0] == c['l_code']:
                length = float(c['length'])
                R_direct = float(elrow[3]) * length
                X_direct = float(elrow[4]) * length
                B_direct = float(elrow[5]) * length
                G_direct = float(elrow[6]) * length
                complex_impedance = R_direct + 1j * X_direct
                impedance_mag = abs(complex_impedance)
                c['impedance line'] = impedance_mag  # then, change the value with the true one
                c['complex impedance'] = complex_impedance
                complex_admittance = G_direct + 1j * B_direct
                c['complex admittance'] = complex_admittance

    ######additional graph for nodes (to check the reference)#####
    # creating the graph of nodes
    for i, elrow in nodes_updated.iterrows():
        grid_nodes.add_node(int(elrow[0]), attr_dict=elrow[1:].to_dict())

    # extracting Reference SS Node
    ref_node = net.get_node_attributes(grid_nodes, 'reference_ss_node')

    # determine the number of substation present in the grid
    SS = []
    for (p, d) in grid_nodes.nodes(data=True):
        SS.append(int(ref_node[p]))  # list with all the reference nodes (substations) with duplicates
    set_SS = set(SS)  # set in order to take each element only once
    num_SS = len(set_SS)  # number of substations in the grid

    #####additional graph for switches (to check which tripped)#####
    # creating the graph of switches
    for i, elrow in switches_updated.iterrows():
        grid_switches.add_edge(int(elrow[2]), int(elrow[3]), attr_dict=elrow[1:].to_dict())

    ######additional graph for loads (to check the importance)#####
    # creating the graph of loads
    for i, elrow in loads_updated.iterrows():
        grid_loads.add_node(int(elrow[4]), attr_dict=elrow[1:].to_dict())  # changed the dict from 3 to 1

    ####################################################################
    ######### IDENTIFY THE BRANCHES AFFECTED BY THE FAULT ##############

    # finding the nodes involved, from the switches that tripped --> TO BE USED FOR THE EXTERNAL CHECK IN "FLISR Algorithm"
    tripped_nodes = []
    for (p, x, d) in grid_switches.edges(data=True):
        if d['switch_tripped'] == 0:
            tripped_nodes.extend([p, x])

    ##### IMPORTANT!!!! in case of bus-tie, it takes also nodes on healthy branch. But successively, they are excluded since there is a path between nodes and SS (feeder switch closed)

    # determine the lost loads, checking connectivity with all substations
    lost_loads = []
    for (p, d) in grid_nodes.nodes(data=True):
        count = 0
        for i in set_SS:
            if not (net.has_path(subgrid, p, i)):
                count += 1
        if count == num_SS:
            lost_loads.append(p)

    ##############################################################################
    ####### eliminate from the graph the switches tripped due to the fault #######
    for (p, x, d) in grid_switches.edges(data=True):
        if d['switch_tripped'] == 0:
            grid_lines.remove_edge(p, x)

            #####################################################################
    #### remove the loads that are permanent lost (in tripped zone) ####
    multiple_loads = []
    for i in lost_loads:  # iterates the load(s) to be restored
        for x in set_SS:  # iterates the substations
            if net.has_path(grid_lines, x, i, ):
                multiple_loads.append(i)  # loads that can be connected

    alive_loads = set(multiple_loads)
    ### define the graph to be used for the algorithm #####
    # remove the tie-unit that connects two alive portions of the network
    for (p, x, d) in grid_switches.edges(data=True):
        if d['status'] == 0:  # identify the tie unit
            if p not in lost_loads and x not in lost_loads:  # verify that both the sides are energized
                grid_lines.remove_edge(p, x)

    #### remove the lost loads from grid_lines ######
    permanent_lost = [val for val in lost_loads if val not in alive_loads]
    alive_grid = grid_lines.copy()
    for (a, b) in alive_grid.nodes(data=True):
        for c in permanent_lost:
            if c in alive_grid.nodes() and a == c:
                alive_grid.remove_node(c)

    if alive_loads:  # list not empty
        print(
            'FLISR : Step 2 : Creating the complete grid with the nodes , lines, and loads of the disconnected grid and identifying the non faulty section of the fauty feeder')
        print(grid_lines.edges())
        all_possible_loads = 0
    else:
        all_possible_loads = 1
        print('all loads have been reconnected')
    return set_SS, alive_loads, grid_lines, grid_switches, grid_loads, grid_nodes, tripped_nodes, all_possible_loads, alive_grid;

    ############## SELECT THE LOAD TO BE RESTORED #######################


def selecting_loads(grid_loads, alive_loads):
    # creating a graph with all the lost loads
    graph_lost_loads = grid_loads.subgraph(alive_loads)
    # create a list with nodes having the highest cost grade
    cost_loads = []
    # increasing grade from 1 until finding the corresponding of lost loads
    i = 1
    while not cost_loads:
        for (p, d) in graph_lost_loads.nodes(data=True):
            if d['cost'] == i:
                cost_loads.append(int(p))
        i = i + 1

    # creating a graph with the max cost grade loads
    graph_cost_loads = graph_lost_loads.subgraph(cost_loads)

    restored_loads = []  # initialize a new list, definitive loads
    # verify if there are more loads with the same grade, so to select the max power
    if len(cost_loads) > 1:
        i = 2e6
        while not restored_loads:
            for (p, d) in graph_cost_loads.nodes(data=True):
                if d['active_p'] == i:
                    restored_loads.append(p)
            i = i - 1
    else:  # if there is only one load, doesn't check the power
        restored_loads = cost_loads

    print('Loads to be restored:', restored_loads)
    return restored_loads


#############################################################################


def flisr_dijkstra(conn_SE, set_SS, alive_loads, grid_lines, grid_switches, grid_loads, grid_nodes, line_type,
                   loads_updated, nodes_updated, alive_grid, general, u1, u2, u3, u4):
    to_be_closed = []
    solution = 0

    vbase = nodes_updated.loc[1, 'vbase']
    pbase = float(general['pbase'])  # define base power, to compute p.u. losses

    ###### call the function to determine the loads to be restored############
    restored_loads = selecting_loads(grid_loads, alive_loads)

    while solution == 0:

        superparameter_ref = 1e9  # re-initialization of the reference superparameter at each FLISR call

        ###### CARRY OUT THE DIJKSTRA ALGORITHM TO FIND THE MINIMUM PATH #############
        for x in set_SS:  # iterates the substations
            for i in restored_loads:  # iterates the load(s) to be restored
                if net.has_path(grid_lines, x,
                                i):  # necessary to verify the existence of path, Dijkstra cannot be empty
                    shortest_path = net.dijkstra_path(grid_lines, x, i,
                                                      'impedance line')  # Dijkstra compute the path according to the specific line impedance
                    shortest_graph = grid_lines.subgraph(
                        shortest_path)  # creating a subgraph with only lines involved in the dijkstra path
                    print('proposed shortest path:', shortest_graph.edges())

                    ######### to verify that the closing path doesn't include two switches (two tie units): network no more radial ###########
                    count = 0  #
                    for (p, m, d) in shortest_graph.edges(data=True):  #
                        for (k, n, v) in grid_switches.edges(data=True):
                            if (p == k and m == n) or (p == n and m == k):
                                if (v['status'] == 0):
                                    count += 1
                    if count > 1:
                        print('No radial path between substation', x, 'and load', i)  #
                        continue  # stop the current for loop, look for a new dijkstra path
                    ##########################################################################################################################

                    # remove from the graph "alive_grid" the open switches and add the proposed tie-switch
                    for (a, b, c) in grid_switches.edges(data=True):
                        for (d, e, f) in alive_grid.edges(data=True):
                            if (a == d and b == e) and c['status'] == 0:  # and ((a,b) in alive_grid.nodes()):
                                alive_grid.remove_edge(a, b)
                            if (a == e and b == d) and c['status'] == 0:  # and ((a,b) in alive_grid.nodes()):
                                alive_grid.remove_edge(b, a)
                    alive_grid = (net.compose(alive_grid,
                                              shortest_graph)).copy()  # merge witht the new shortest graph, in order to include the proposed closed tie-switch

                    ##### INITIALIZATION OF STATE ESTIMATION MEASUREMENTS ####
                    error = 0
                    power_loss = 0
                    pload = 0
                    min_line_relat = [9999, 1, 1, 9999, 1, 1, 9999, 1, 1]

                    #### SUB-DIVISION OF THE NETWORK; CREATING ONE GRAPH FOR EACH SUBSTATION #####
                    for z in set_SS:
                        SS_alive_grid = alive_grid.copy()
                        for (y, t) in SS_alive_grid.nodes(data=True):
                            if (not net.has_path(SS_alive_grid, z, y)) and y in SS_alive_grid.nodes():
                                SS_alive_grid.remove_node(y)
                        print('grid', z, 'composed by:', SS_alive_grid.edges())

                        ### CALL THE FUNCTION TO DEFINE THE INPUT FOR THE STATE ESTIMATOR
                        graph_to_SE, mapping, reverse_mapping = graph_mapping_for_SE(SS_alive_grid, grid_nodes)
                        griddata = convert_graph_to_SE_linedata(graph_to_SE, line_type, grid_nodes, pbase, vbase)
                        measdata_to_SE, pload = Measurements_to_SE(mapping, grid_loads, SS_alive_grid, pbase, pload)

                        ### STATE ESTIMATION FUNCTION - for the portion of the grid
                        dssedata1 = DSSEVRI1(griddata, measdata_to_SE)
                        temp1 = np.zeros((dssedata1.nstate, 1))
                        temp2 = np.zeros((dssedata1.nstate, 1))
                        for h in range(dssedata1.nstate):
                            for key in reverse_mapping.keys():
                                if int(dssedata1.dsseinstance[h].itemid1) == key:
                                    temp1[h, 0] = reverse_mapping[key]
                                    # dssedata2.dsseinstance[h].itemid1=reverse_mapping[key]
                                if int(dssedata1.dsseinstance[h].itemid2) == key:
                                    temp2[h, 0] = reverse_mapping[key]
                                    # dssedata2.dsseinstance[h].itemid2=reverse_mapping[key]

                        for h in range(dssedata1.nstate):
                            dssedata1.dsseinstance[h].itemid1 = temp1[h, 0]
                            dssedata1.dsseinstance[h].itemid2 = temp2[h, 0]
                        error, power_loss, min_line_relat = check_dijkstra(dssedata1, grid_nodes, nodes_updated, x, i,
                                                                           shortest_graph, set_SS, loads_updated,
                                                                           line_type, grid_switches, SS_alive_grid,
                                                                           error, power_loss, min_line_relat, pbase)
                    if error == 0:

                        ### GET STATE ESTIMATION RESULTS!!!!!
                        # carry out computation of path losses, and add the weighting factor u1

                        weighted_losses = u1 * power_loss / pload
                        print('LOSSES INDICATOR:', power_loss / pload, 'and with pload:', pload)
                        print('ATTENTION!!!!!!! substation', x, 'load', i, 'weighted_losses', weighted_losses)
                        # compute the utilization parameter by min((In - I)/In). the minimum is on all lines. bring to denominator and add the wighting factor u2 and
                        weighted_min_line = u2 / min_line_relat[0] + u3 / min_line_relat[3] + u4 / min_line_relat[6]
                        print('ATTENTION!!!!!!! These are the most consumed lines', min_line_relat,
                              'and the total parameter is:', weighted_min_line)
                        print('without u factors:',
                              1 / min_line_relat[0] + 1 / min_line_relat[3] + 1 / min_line_relat[6])

                        superparameter = weighted_losses + weighted_min_line  # otherwise, consider both parameters

                        print('ATTENTION!!!!!!! This is the superparameter:', superparameter)

                        if superparameter < superparameter_ref:
                            superparameter_ref = superparameter
                            graph_path = grid_switches.subgraph(
                                shortest_path)  # create a subgraph with edges having switches
                            subst = x
                            load = i
                            final_path = shortest_path
                            for (a, b, c) in graph_path.edges(data=True):
                                if c['status'] == 1:  # keep only the open switches (to be closed)
                                    graph_path.remove_edge(a, b)
                            solution = 1  # solution found

        ####### this loads cannot be connected, look for other loads ###############
        if solution == 0:
            for w in restored_loads:  # remove the inspected loads from the selected list
                alive_loads.remove(w)
            if alive_loads:  # if the list is not empty, look for new loads and repeat the dijkstra
                restored_loads = selecting_loads(grid_loads, alive_loads)
            else:  # otherwise stop the process
                solution = 1

    try:
        to_be_closed = list(net.get_edge_attributes(graph_path, 'name').values())
        print(
            'FLISR : Step 3 : identifying the Optimal reconfigurable path for restoring the non faulty section of faulty feeder')
        all_possible_loads = 0
    except:
        print('no more load can be reconnected with a suitable path')
        all_possible_loads = 1
        final_path = []
        load = []
        subst = []
    return to_be_closed, final_path, load, subst, all_possible_loads;


###############################################################################
####### FUNCTION TO CHECK THE PRESENCE OF TRIPPED SWITCHES ####################
def check_fault(conn_txt, is_fault_identified):
    switches_updated = fetch_switches_flisr_table(conn_txt)
    for i, elrow in switches_updated.iterrows():
        grid_switches.add_edge(elrow[2], elrow[3], attr_dict=elrow[1:].to_dict())
    check_tripped = []
    for (p, x, d) in grid_switches.edges(data=True):
        if d['switch_tripped'] == 0:
            check_tripped.extend([p, x])
            is_fault_identified = 1
    return check_tripped, is_fault_identified


###############################################################################
###### FUNCTION TO CHECK THE RESPECT OF NETWORK RADIALITY AND LINE CAPABILITY##########
def check_dijkstra(dssedata, grid_nodes, nodes_updated, x, i, shortest_graph, set_SS, loads_updated, line_type,
                   grid_switches, SS_alive_grid, error, power_loss, min_line_relat, pbase):
    #########################################################################
    # HERE THE VOLTAGE LEVEL CHECK TAKES PLACE, of the whole grid
    # import outcomes from STATE ESTIMATION
    for o, elrow in nodes_updated.iterrows():  # iterate the nodes, all the grid must be checked
        for q in range(dssedata.nstate):
            if int(dssedata.dsseinstance[q].itemid1) == elrow[0] and int(
                    dssedata.dsseinstance[q].mtype) == 1:  # it should be the LN voltage, TO BE CHECKED!!!
                if (float(dssedata.dsseinstance[q].estimationvalue) + 3 * float(
                        dssedata.dsseinstance[q].estimationaccuracy)) > 1.1 or (
                        float(dssedata.dsseinstance[q].estimationvalue) + 3 * float(dssedata.dsseinstance[
                                                                                        q].estimationaccuracy)) < 0.9:  # removed the conversion to absolute value: * float(elrow[4]): #Check voltage level in range 0.9 -> 1.1
                    print('Voltage level exceeds the limits at node', elrow[1], ' being ', (
                                float(dssedata.dsseinstance[q].estimationvalue) + 3 * float(
                            dssedata.dsseinstance[q].estimationaccuracy)),
                          '% so this network configuration is not acceptable')
                    error = 1
                    return error, power_loss, min_line_relat
    # print('In this network configuration, voltage limits are respected for all the nodes')

    ####### load the nodes voltage
    voltages_complete = net.get_node_attributes(grid_nodes, 'vbase')

    # HERE THE CURRENT CAPACITY CHECK TAKES PLACE, of the whole grid

    for (a, b, c) in SS_alive_grid.edges(data=True):
        Ibase = pbase / (math.sqrt(3) * float(voltages_complete[a]))
        for o, elrow in line_type.iterrows():
            if elrow[0] == c['l_code']:
                amax = elrow[1]
        for r in range(dssedata.nstate):

            # acquire voltage magnitude and phase (with uncertainties), for node a and b
            if int(dssedata.dsseinstance[r].itemid1) == a and int(dssedata.dsseinstance[
                                                                      r].mtype) == 1:  # and int(dssedata.dsseinstance[r].phase)=='AN': #it should be the LN voltage, TO BE CHECKED!!! phase A taken only as reference (everything symmetrical)
                V1_mag = float(dssedata.dsseinstance[r].estimationvalue) * float(
                    voltages_complete[a])  # get the p.u. voltage and multiply by nominal voltage
                u_V1_mag = float(dssedata.dsseinstance[r].estimationaccuracy) * float(voltages_complete[a])
            #                print('volt+acc:',V1_mag, u_V1_mag)

            if int(dssedata.dsseinstance[r].itemid1) == a and int(
                    dssedata.dsseinstance[r].mtype) == 3:  # and int(dssedata.dsseinstance[r].phase)==A:
                V1_phase = float(dssedata.dsseinstance[r].estimationvalue)
                u_V1_phase = float(dssedata.dsseinstance[r].estimationaccuracy)
            #                print('phase volt+acc:',V1_phase, u_V1_phase)

            if int(dssedata.dsseinstance[r].itemid1) == b and int(
                    dssedata.dsseinstance[r].mtype) == 1:  # and int(dssedata.dsseinstance[r].phase)==A:
                V2_mag = float(dssedata.dsseinstance[r].estimationvalue) * float(voltages_complete[a])
                u_V2_mag = float(dssedata.dsseinstance[r].estimationaccuracy) * float(voltages_complete[a])
            #                print('volt+acc:',V2_mag, u_V2_mag)

            if int(dssedata.dsseinstance[r].itemid1) == b and int(
                    dssedata.dsseinstance[r].mtype) == 3:  # and int(dssedata.dsseinstance[r].phase)==A:
                V2_phase = float(dssedata.dsseinstance[r].estimationvalue)
                u_V2_phase = float(dssedata.dsseinstance[r].estimationaccuracy)
            #                print('phase volt+acc:',V2_phase, u_V2_phase)

            # acquire current magnitude and phase (with uncertainties) by comparign nodes a and b with itemid1 and itemid2 (considering both the combinations)
            if ((int(dssedata.dsseinstance[r].itemid1) == a and int(dssedata.dsseinstance[r].itemid2) == b) or (
                    int(dssedata.dsseinstance[r].itemid1) == b and int(dssedata.dsseinstance[r].itemid2) == a)) and int(
                    dssedata.dsseinstance[r].mtype) == 5:
                current_mag = float(dssedata.dsseinstance[
                                        r].estimationvalue) * Ibase  # get the p.u. voltage and multiply by nominal voltage
                u_current_mag = float(dssedata.dsseinstance[r].estimationaccuracy) * Ibase
            #                print('curr mag + acc:',current_mag, u_current_mag, 'nodes:',a, b)

            if ((int(dssedata.dsseinstance[r].itemid1) == a and int(dssedata.dsseinstance[r].itemid2) == b) or (
                    int(dssedata.dsseinstance[r].itemid1) == b and int(dssedata.dsseinstance[r].itemid2) == a)) and int(
                    dssedata.dsseinstance[r].mtype) == 7:
                current_phase = float(dssedata.dsseinstance[r].estimationvalue)
                u_current_phase = float(dssedata.dsseinstance[r].estimationaccuracy)
        #                print('curr phase + acc:',current_phase, u_current_phase, 'nodes:',a,b)

        #        transform the voltage measurement from polar to rectangular form
        V1_rect = cmath.rect(V1_mag, V1_phase)
        V2_rect = cmath.rect(V2_mag, V2_phase)

        ###complex impedance and admittance
        Z = c['complex impedance']
        Y = c['complex admittance']

        if (current_mag + 3 * u_current_mag) > amax:  # for each edge, check that the current limit is respected
            print('too much current:', current_mag + 3 * u_current_mag, 'in percentage:',
                  (current_mag + 3 * u_current_mag) / amax)
            print('Line current exceeds the thermal limits at nodes', a, 'and', b,
                  ':this network configuration is not acceptable')
            error = 1
            return error, power_loss, min_line_relat

        # compute the power losses in each line
        power_loss += np.real(V1_rect * (np.conjugate(V1_rect * Y / 2)) + (V1_rect - V2_rect) * (
            np.conjugate((V1_rect - V2_rect) / Z)) + V2_rect * (
                                  np.conjugate(V2_rect * Y / 2)))  # sum the power losses of each edge

        # compute line exploitation by current reference
        line_relat = (amax - current_mag) / amax
        if line_relat < min_line_relat[0]:  # if supergraph.has_edge(a,b):
            min_line_relat[6] = min_line_relat[3]
            min_line_relat[7] = min_line_relat[4]
            min_line_relat[8] = min_line_relat[5]
            min_line_relat[3] = min_line_relat[0]
            min_line_relat[4] = min_line_relat[1]
            min_line_relat[5] = min_line_relat[2]
            min_line_relat[0] = line_relat
            min_line_relat[1] = a
            min_line_relat[2] = b

        elif line_relat < min_line_relat[3]:
            min_line_relat[6] = min_line_relat[3]
            min_line_relat[7] = min_line_relat[4]
            min_line_relat[8] = min_line_relat[5]
            min_line_relat[3] = line_relat
            min_line_relat[4] = a
            min_line_relat[5] = b

        elif line_relat < min_line_relat[6]:
            min_line_relat[6] = line_relat
            min_line_relat[7] = a
            min_line_relat[8] = b

    # print('In this network configuration, current limits are respected for all the lines')
    #
    # print('line capability between substation',x,'and load',i,'is acceptable')
    return error, power_loss, min_line_relat

#functions to map the single feeders accordingly to state estimation function

def graph_mapping_for_SE(SS_alive_grid, grid_nodes):
    ref_node = net.get_node_attributes(grid_nodes, 'reference_ss_node')
    mapping = {}
    keys = SS_alive_grid.nodes()
    count = 0
    for bus in keys:
        if (bus == ref_node[bus]):
            mapping[bus] = 0
        else:
            count = count + 1
            mapping[bus] = count

    graph_to_SE = net.relabel_nodes(SS_alive_grid, mapping)
    reverse_keys = list(mapping.values())
    reverse_values = list(mapping.keys())
    reverse_mapping = dict(zip(reverse_keys, reverse_values))
    return graph_to_SE, mapping, reverse_mapping


############################ function for converting graph to topology data ########################################

def convert_graph_to_SE_linedata(graph_to_SE, line_type, grid_nodes, pbasegen, vbase):
    ####### load the nodes voltage
    voltages_complete = net.get_node_attributes(grid_nodes, 'vbase')

    i = 0
    griddata1 = griddata()  # pbase is 100 kW and gridtype
    griddata1.pbase = pbasegen;
    griddata1.vbasenom = vbase;
    griddata1.gtype = 1;
    griddata1.nline = 0;  # initialization to zero
    griddata1.nnode = 0;  # initialization to zero #iteration to populate the griddata1.nodes class
    for (a, b) in graph_to_SE.nodes(data=True):
        griddata1.addnode(a, voltages_complete[a])
        griddata1.nnode = griddata1.nnode + 1  # iteration to populate the griddata1.edges class
    for (a, b, c) in graph_to_SE.edges(data=True):
        for o, elrow in line_type.iterrows():
            if elrow[0] == c['l_code']:
                griddata1.addline(c['id'], 1, 1, 1, a, b, elrow[1])  # added id_line and max current value

                griddata1.line[i].addphaseP11(float(elrow[3] * c['length']), float(elrow[4] * c['length']),
                                              float(elrow[5] * c['length']), float(elrow[6] * c['length']))
                griddata1.line[i].addphaseP12(float(elrow[7] * c['length']), float(elrow[8] * c['length']),
                                              float(elrow[9] * c['length']), float(elrow[10] * c['length']))
                griddata1.nline = griddata1.nline + 1
                i += 1
            # positive sequence in Ohm (later will be converted by the estimator in pu)
            # assuming r_pos_m1 is already multiplied by the length
    return griddata1


####################################### function to build the measurement object to be given to the SE ##################################

def Measurements_to_SE(mapping, grid_loads, SS_alive_grid, pbase, pload):
    Nmeas = 0;
    measdata_to_SE = measdata()

    ############# retrieve the active_power and reactive_power from the graph of loadst mimicing the power injection values (i real time)####
    active_power_complete = net.get_node_attributes(grid_loads, 'active_p')
    reactive_power_complete = net.get_node_attributes(grid_loads, 'reactive_q')

    #  Setting the measurement class object of SE with the voltage mag and anng at substation buses and injection values at all other buses
    for (node, b) in SS_alive_grid.nodes(data=True):

        if (mapping[node] == 0):  # include the voltage measurement for the SS node or nodes with 0 injections
            measdata_to_SE.addmeasurementinstance(Nmeas, 1, 1, mapping[node], 0.01,
                                                  1)  # append with a voltage magnitude instance
            Nmeas = Nmeas + 1;
            measdata_to_SE.addmeasurementinstance(Nmeas, 3, 1, mapping[node], 0.01,
                                                  0)  # append with a voltage angle instance
            Nmeas = Nmeas + 1;
        elif (active_power_complete[node] != 0 and reactive_power_complete[node] != 0):
            measdata_to_SE.addmeasurementinstance(Nmeas, 9, 1, mapping[node], 0.01414, active_power_complete[
                node] / pbase)  # append with an active power injection instance
            Nmeas = Nmeas + 1;
            pload += active_power_complete[node]
            measdata_to_SE.addmeasurementinstance(Nmeas, 11, 1, mapping[node], 0.01414, reactive_power_complete[
                node] / pbase)  # append with a reactive power injection instance
            Nmeas = Nmeas + 1;

    measdata_to_SE.nmeas = Nmeas
    return measdata_to_SE, pload

#update data to database, functions:

def update_line_flisr_table(conn_txt, lines, mode):
    if mode == 1:
        SQLtext = "";
        for i, elrow in lines.iterrows():
            SQLtext = SQLtext + " UPDATE networktopology.lines"
            SQLtext = SQLtext + " SET name='" + str(elrow[0]) + "',l_code='" + str(elrow[1]) + "',status='" + str(
                elrow[2]) + "',node1='" + str(elrow[3]) + "',node2='" + str(elrow[4]) + "',length='" + str(
                elrow[6]) + "'"
            SQLtext = SQLtext + " WHERE line_id='" + str(elrow[5]) + "';"
    else:
        SQLtext = "";
        for i, elrow in lines.iterrows():
            SQLtext = SQLtext + " INSERT INTO networktopology.lines(name,l_code,status,node1,node2,line_id,length) "
            SQLtext = SQLtext + " VALUES('" + str(elrow[0]) + "','" + str(elrow[1]) + "','" + str(
                elrow[2]) + "','" + str(elrow[3]) + "','" + str(elrow[4]) + "','" + str(elrow[5]) + "','" + str(
                elrow[6]) + "');\n"

    update_time = datetime.utcnow()
    try:
        # DB connection parameters, change those if needed
        conn = psycopg2.connect(conn_txt)
    #    	print("trying to connect to the database to update LINE_TABLE")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)
    else:
        #        print('Connected!')

        cur = conn.cursor()

        try:
            cur.execute(SQLtext)
            conn.commit()
        #            print(" trying to update LINE_TABLE" )
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when writing DB')
            sys.exit(1)
        finally:
            print('successfully updated LINE_TABLE, closing the connection')
            # finally we close the connection
            conn.close()
    return update_time


def update_nodes_flisr_table(conn_txt, nodes, mode):
    if mode == 1:
        SQLtext = "";
        for i, elrow in nodes.iterrows():
            SQLtext = SQLtext + " UPDATE networktopology.nodes"
            SQLtext = SQLtext + " SET name='" + str(elrow[0]) + "',groupnode='" + str(elrow[1]) + "',vbase='" + str(
                elrow[3]) + "'"
            SQLtext = SQLtext + " WHERE node_id='" + str(elrow[2]) + "';"
    else:
        SQLtext = "";
        for i, elrow in nodes.iterrows():
            SQLtext = SQLtext + " INSERT INTO networktopology.nodes(name,groupnode,node_id,vbase) "
            SQLtext = SQLtext + " VALUES('" + str(elrow[0]) + "','" + str(elrow[1]) + "','" + str(
                elrow[2]) + "','" + str(elrow[3]) + "');\n"

    update_time = datetime.utcnow()
    try:
        # DB connection parameters, change those if needed
        conn = psycopg2.connect(conn_txt)
    #    	print("trying to connect to the database to update NODES_TABLE")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)
    else:
        #        print('Connected!')

        cur = conn.cursor()

        try:
            cur.execute(SQLtext)
            conn.commit()
        #            print(" trying to populate NODES_TABLE" )
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when writing DB')
            sys.exit(1)
        finally:
            print('successfully updated NODES_TABLE, closing the connection')
            # finally we close the connection
            conn.close()
    return update_time


def update_switches_flisr_table(conn_txt, switches, mode):
    if mode == 1:
        SQLtext = "";
        for i, elrow in switches.iterrows():
            SQLtext = SQLtext + " UPDATE networktopology.switches"
            SQLtext = SQLtext + " SET name='" + str(elrow[1]) + "',switch_tripped='" + str(
                elrow[2]) + "',status='" + str(elrow[3]) + "',node1='" + str(elrow[4]) + "',node2='" + str(
                elrow[5]) + "'"
            SQLtext = SQLtext + " WHERE switch_id='" + str(elrow[0]) + "';"

    else:
        SQLtext = "";
        for i, elrow in switches.iterrows():
            SQLtext = SQLtext + " INSERT INTO networktopology.switches(switch_id,name,switch_tripped,status,node1,node2) "
            SQLtext = SQLtext + " VALUES('" + str(elrow[0]) + "','" + str(elrow[1]) + "','" + str(
                elrow[2]) + "','" + str(elrow[3]) + "','" + str(elrow[4]) + "','" + str(elrow[5]) + "');\n"

    update_time = datetime.utcnow()
    try:
        # DB connection parameters, change those if needed
        conn = psycopg2.connect(conn_txt)
    #    	print("trying to connect to the database to update SWITCHES_TABLE")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)
    else:
        #        print('Connected!')

        cur = conn.cursor()

        try:
            cur.execute(SQLtext)
            conn.commit()
        #            print(" trying to populate SWITCHES_TABLE" )
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when writing DB')
            sys.exit(1)
        finally:
            print('successfully updated SWITCHES_TABLE, closing the connection')
            # finally we close the connection
            conn.close()
    return update_time


def update_ltype_flisr_table(conn_txt, ltype, mode):
    if mode == 1:
        SQLtext = "";
        for i, elrow in ltype.iterrows():
            SQLtext = SQLtext + " UPDATE networktopology.ltype"
            SQLtext = SQLtext + " SET name='" + str(elrow[1]) + "',amax='" + str(elrow[2]) + "',r_pos_m='" + str(
                elrow[3]) + "',x_pos_m='" + str(elrow[4]) + "',b_pos_m='" + str(elrow[5]) + "',g_pos_m='" + str(
                elrow[6]) + "',r_zero_m='" + str(elrow[7]) + "',x_zero_m='" + str(elrow[8]) + "',b_zero_m='" + str(
                elrow[9]) + "',g_zero_m='" + str(elrow[10]) + "'"
            SQLtext = SQLtext + " WHERE l_code='" + str(elrow[0]) + "';"

    else:
        SQLtext = "";
        for i, elrow in ltype.iterrows():
            SQLtext = SQLtext + " INSERT INTO networktopology.ltype(lcode,name,r_pos_m,x_pos_m,b_pos_m,g_pos_m,r_zero_m,x_zero_m,b_zero_m,g_zero_m,amax) "
            SQLtext = SQLtext + " VALUES('" + str(elrow[0]) + "','" + str(elrow[1]) + "','" + str(
                elrow[2]) + "','" + str(elrow[3]) + "','" + str(elrow[4]) + "','" + str(elrow[5]) + "','" + str(
                elrow[6]) + "','" + str(elrow[7]) + "','" + str(elrow[8]) + "','" + str(elrow[9]) + "','" + str(
                elrow[10]) + "');\n"

    update_time = datetime.utcnow()
    try:
        # DB connection parameters, change those if needed
        conn = psycopg2.connect(conn_txt)
    #    	print("trying to connect to the database to update LINE_TYPE_TABLE")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)
    else:
        #        print('Connected!')

        cur = conn.cursor()

        try:
            cur.execute(SQLtext)
            conn.commit()
        #            print(" trying to populate LINE_TYPE_TABLE" )
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when writing DB')
            sys.exit(1)
        finally:
            print('successfully updated LINE_TYPE_TABLE, closing the connection')
            # finally we close the connection
            conn.close()
    return update_time


def update_loads_flisr_table(conn_txt, loads, mode):
    if mode == 1:
        SQLtext = "";
        for i, elrow in loads.iterrows():
            SQLtext = SQLtext + " UPDATE networktopology.injections"
            SQLtext = SQLtext + " SET name='" + str(elrow[1]) + "',nominalpf='" + str(elrow[2]) + "',node_id='" + str(
                elrow[3]) + "',cost_flisr='" + str(elrow[4]) + "',active_p='" + str(elrow[5]) + "',reactive_q='" + str(
                elrow[6]) + "'"
            SQLtext = SQLtext + " WHERE injection_id='" + str(elrow[0]) + "';"
    else:
        SQLtext = "";
        for i, elrow in loads.iterrows():
            SQLtext = SQLtext + " INSERT INTO networktopology.injections(injection_id,name,nominalpf,node_id,cost_flisr, active_p, reactive_q) "
            SQLtext = SQLtext + " VALUES('" + str(elrow[0]) + "','" + str(elrow[1]) + "','" + str(
                elrow[2]) + "','" + str(elrow[3]) + "','" + str(elrow[4]) + "','" + str(elrow[5]) + "','" + str(
                elrow[6]) + "');\n"

    update_time = datetime.utcnow()
    try:
        # DB connection parameters, change those if needed
        conn = psycopg2.connect(conn_txt)
    #    	print("trying to connect to the database to update LOADS_TABLE")
    except psycopg2.OperationalError as e:
        #    	print('Unable to connect!\n{0}').format(e)
        sys.exit(1)
    else:
        #        print('Connected!')

        cur = conn.cursor()

        try:
            cur.execute(SQLtext)
            conn.commit()
        #            print(" trying to populate LOADS_TABLE" )
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when writing DB')
            sys.exit(1)
        finally:
            print('successfully updated LOADS_TABLE, closing the connection')
            # finally we close the connection
            conn.close()
    return update_time


def update_tripped_switches_coloumn(conn_txt, switches):
    SQLtext = "";
    for i, elrow in switches.iterrows():
        SQLtext = SQLtext + " UPDATE networktopology.switches"
        SQLtext = SQLtext + " SET switch_tripped='" + str(elrow[5]) + "'"
        SQLtext = SQLtext + " WHERE switch_id='" + str(elrow[0]) + "';"

    update_time = datetime.utcnow()
    try:
        # DB connection parameters, change those if needed
        conn = psycopg2.connect(conn_txt)
    #    	print("trying to connect to the database to update SWITCHES_TABLE")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)
    else:
        #        print('Connected!')

        cur = conn.cursor()

        try:
            cur.execute(SQLtext)
            conn.commit()
        #            print(" trying to update tripped switches in switches table" )
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when writing DB')
            sys.exit(1)
        finally:
            print('successfully updated tripped switches in switches table, closing the connection')
            # finally we close the connection
            conn.close()
    return update_time


def update_switches_status_coloumn(conn_txt, switches):
    SQLtext = "";
    for i, elrow in switches.iterrows():
        SQLtext = SQLtext + " UPDATE networktopology.switches"
        SQLtext = SQLtext + " SET status='" + str(elrow[4]) + "'"
        SQLtext = SQLtext + " WHERE switch_id='" + str(elrow[0]) + "';"

    update_time = datetime.utcnow()
    try:
        # DB connection parameters, change those if needed
        conn = psycopg2.connect(conn_txt)
    #    	print("trying to connect to the database to update SWITCHES_TABLE")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)
    else:
        #        print('Connected!')

        cur = conn.cursor()

        try:
            cur.execute(SQLtext)
            conn.commit()
        #            print(" trying to update status switches in switches table" )
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when writing DB')
            sys.exit(1)
        finally:
            print('successfully updated tripped switches in switches table, closing the connection')
            # finally we close the connection
            conn.close()
    return update_time


def update_node_reference_coloumn(conn_txt, switches):
    SQLtext = "";
    for i, elrow in switches.iterrows():
        SQLtext = SQLtext + " UPDATE networktopology.nodes"
        SQLtext = SQLtext + " SET groupnode='" + str(elrow[2]) + "'"
        SQLtext = SQLtext + " WHERE name='" + str(elrow[1]) + "';"

    update_time = datetime.utcnow()
    try:
        # DB connection parameters, change those if needed
        conn = psycopg2.connect(conn_txt)
    #    	print("trying to connect to the database to update SWITCHES_TABLE")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)
    else:
        #        print('Connected!')

        cur = conn.cursor()

        try:
            cur.execute(SQLtext)
            conn.commit()
        #            print(" trying to update tripped switches in switches table" )
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when writing DB')
            sys.exit(1)
        finally:
            print('successfully updated tripped switches in switches table, closing the connection')
            # finally we close the connection
            conn.close()
    return update_time


#fetch data from database, functions:

def fetch_switches_flisr_table(conn_txt):
    try:
        # DB connection parameters, change those if needed
        conn = psycopg2.connect(conn_txt)
    #    	print("trying to connect to the database to retrieve SWITCHES_TABLE")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)
    else:
        #        print('Connected!')

        cur = conn.cursor()
        SQLtext = ""
        SQLtext += " SELECT switch_id,name,node1,node2,status,switch_tripped FROM networktopology.switches"

        try:
            cur.execute(SQLtext)
            #            print(" retrieving the switch_id,name,node1,node2,status,switch_tripped from SWITCHES_TABLE in SAU DB" )
            switch_data = cur.fetchall()
            switches_updated = pd.DataFrame(data=switch_data,
                                            columns=['id', 'name', 'node1', 'node2', 'status', 'switch_tripped'])
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when reading DB for switch data')
            sys.exit(1)
        finally:
            print('successfully retrieved updated switch data, closing the connection')
            # finally we close the connection
            conn.close()
    return switches_updated


def fetch_nodes_flisr_table(conn_txt):
    try:
        # DB connection parameters, change those if needed
        conn = psycopg2.connect(conn_txt)
    #    	print("trying to connect to the database to retrieve NODES_TABLE")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)
    else:
        #        print('Connected!')

        cur = conn.cursor()
        SQLtext = ""
        SQLtext += " SELECT node_id, name,groupnode,vbase FROM networktopology.nodes"

        try:
            cur.execute(SQLtext)
            #            print(" retrieving the id,name,node_id,groupnode,node_energized,vbase from NODES_TABLE in SAU DB" )
            node_data = cur.fetchall()
            node_updated = pd.DataFrame(data=node_data, columns=['nodeid', 'name', 'reference_ss_node', 'vbase'])
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when reading DB for node data')
            sys.exit(1)
        finally:
            print('successfully retrieved updated node data, closing the connection')
            # finally we close the connection
            conn.close()
    return node_updated


def fetch_lines_flisr_table(conn_txt):
    try:
        # DB connection parameters, change those if needed
        conn = psycopg2.connect(conn_txt)
    #    	print("trying to connect to the database to retrieve LINES_TABLE")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)
    else:
        #        print('Connected!')

        cur = conn.cursor()
        SQLtext = ""
        SQLtext += " SELECT line_id,name,node1,node2,status,l_code,length FROM networktopology.lines"

        try:
            cur.execute(SQLtext)
            #            print(" retrieving the id,name,node1,node2,status,line_id,l_code,length from LINES_TABLE in SAU DB" )
            line_data = cur.fetchall()
            line_updated = pd.DataFrame(data=line_data,
                                        columns=['id', 'name', 'node1', 'node2', 'status', 'l_code', 'length'])
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when reading the DB')
            sys.exit(1)
        finally:
            print('successfully retrieved updated line data, closing the connection')
            # finally we close the connection
            conn.close()
    return line_updated


def fetch_loads_flisr_table(conn_txt):
    try:
        # DB connection parameters, change those if needed
        conn = psycopg2.connect(conn_txt)
    #    	print("trying to connect to the database to retrieve INJECTIONS_TABLE")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)
    else:
        #        print('Connected!')

        cur = conn.cursor()
        SQLtext = ""
        SQLtext += " SELECT injection_id,name,cost_flisr,nominalpf,node_id,active_p,reactive_q FROM networktopology.injections"

        try:
            cur.execute(SQLtext)
            #            print(" retrieving the injection_id,name,nominals,injection_type,cost_flisr,status,nominalpf,node_id from INJECTIONS_TABLE in SAU DB" )
            load_data = cur.fetchall()
            load_updated = pd.DataFrame(data=load_data,
                                        columns=['id', 'name', 'cost', 'nominalpf', 'node', 'active_p', 'reactive_q'])
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when reading DB')
            sys.exit(1)
        finally:
            print('successfully retrieved updated load data, closing the connection')
            # finally we close the connection
            conn.close()
    return load_updated


def fetch_lines_type_table(conn_txt):
    last_fetch_time_ltype = datetime.utcnow()
    try:
        # DB connection parameters, change those if needed
        conn = psycopg2.connect(conn_txt)
    #    	print("trying to connect to the database to retrieve LINES_TYPE_TABLE")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)
    else:
        #        print('Connected!')

        cur = conn.cursor()
        SQLtext = ""
        SQLtext += " SELECT l_code,amax,name,r_pos_m,x_pos_m,b_pos_m,g_pos_m,r_zero_m,x_zero_m,b_zero_m,g_zero_m FROM networktopology.ltype"

        try:
            cur.execute(SQLtext)
            #            print(" retrieving the LINE_TYPE_TABLE  from database" )
            ltype_data = cur.fetchall()
            line_type = pd.DataFrame(data=ltype_data,
                                     columns=['lcode', 'amax', 'name', 'r_pos_m', 'x_pos_m', 'b_pos_m', 'g_pos_m',
                                              'r_zero_m', 'x_zero_m', 'b_zero_m', 'g_zero_m'])
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when writing DB')
            sys.exit(1)
        finally:
            print('successfully retrieved line type data, closing the connection')
            # finally we close the connection
            conn.close()
    return line_type, last_fetch_time_ltype

#update flags

def update_fault_detection_flag(is_switch_tripped, conn_txt):
    SQLtext = "";
    if is_switch_tripped == 1:
        SQLtext = "";
        # Try connection to DB
        try:
            # DB connection parameters, change those if needed
            conn = psycopg2.connect(conn_txt)
        #        	print("trying to connect to the database for updating flag FLISR_FAULT_DETECTED")
        except psycopg2.OperationalError as e:
            print('Unable to connect!\n{0}').format(e)
            sys.exit(1)
        else:
            #            print('Connected!')
            # we truncate (delete) the data already present in DB, so that we do not have overlappings.  If you do not want to do that delete the following line.
            # But consider that the indexes used for "Id" start always from 1, so we may have some double items and the DB will not like it as Id is defined as primary key
            # To avoid such problem, you can adapt the code by reading from the DB the biggest Id and to start from the following
            cur = conn.cursor()
            SQLtext += " INSERT INTO management.algorithm_log(algorithm_id,timestamp,log_message) "
            SQLtext += " VALUES('4','" + str(
                datetime.utcnow()) + " ','FLISR_FAULT_DETECTED: faults discovered in the grid');\n "
            # we also store the query in a text file in order to be further manually adapted (this file may be opened for instance with PGadmin)
        # we try to execute and commit the query to the DB
        #            text_file = open("U:\\ACS-Internal\\Personal_Folders\\asa\\Projects\\10_SAU_Graph_based_SR\\Simulation Files\\OutputSQL_historian.sql", "w")
        #            text_file.write(SQLtext)
        #            text_file.close()
        try:
            cur.execute(SQLtext)
            conn.commit()
        #        	print(" trying to update flag FLISR_FAULT_DETECTED " )
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when writing DB')
            sys.exit(1)
        finally:
            print('successfully updated the flag FLISR_FAULT_DETECTED')
            # finally we close the connection
            conn.close()

#Database flags updates:

def update_fault_identification_flag(is_fault_identified, conn_txt):
    if is_fault_identified == 1:
        SQLtext = "";
        # Try connection to DB
        try:
            # DB connection parameters, change those if needed
            conn = psycopg2.connect(conn_txt)
        #        	print("trying to connect to the database for updating flag FLISR_FAULT_IDENTIFIED ")
        except psycopg2.OperationalError as e:
            print('Unable to connect!\n{0}').format(e)
            sys.exit(1)
        else:
            #            print('Connected!')
            # we truncate (delete) the data already present in DB, so that we do not have overlappings.  If you do not want to do that delete the following line.
            # But consider that the indexes used for "Id" start always from 1, so we may have some double items and the DB will not like it as Id is defined as primary key
            # To avoid such problem, you can adapt the code by reading from the DB the biggest Id and to start from the following
            cur = conn.cursor()
            SQLtext += " INSERT INTO management.algorithm_log(algorithm_id,timestamp,log_message) "
            SQLtext += " VALUES('4','" + str(
                datetime.utcnow()) + " ','FLISR_FAULT_IDENTIFIED: the fault has been isolated, the algorithm may start');\n "

        try:
            cur.execute(SQLtext)
            conn.commit()
        #        	print(" trying to update flag FLISR_FAULT_IDENTIFIED" )
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when writing DB')
            sys.exit(1)
        finally:
            print('successfully updated the flag FLISR_FAULT_IDENTIFIED')
            # finally we close the connection
            conn.close()


def update_FLISR_operation_flag(is_FLISR_in_operation, conn_txt):
    if is_FLISR_in_operation == 1:
        SQLtext = "";
        # Try connection to DB
        try:
            # DB connection parameters, change those if needed
            conn = psycopg2.connect(conn_txt)
        #        	print("trying to connect to the database for updating flag FLISR_IN_OPERATION ")
        except psycopg2.OperationalError as e:
            print('Unable to connect!\n{0}').format(e)
            sys.exit(1)
        else:
            #            print('Connected!')
            # we truncate (delete) the data already present in DB, so that we do not have overlappings.  If you do not want to do that delete the following line.
            # But consider that the indexes used for "Id" start always from 1, so we may have some double items and the DB will not like it as Id is defined as primary key
            # To avoid such problem, you can adapt the code by reading from the DB the biggest Id and to start from the following
            cur = conn.cursor()
            SQLtext += " INSERT INTO management.algorithm_log(algorithm_id,timestamp,log_message) "
            SQLtext += " VALUES('4','" + str(
                datetime.utcnow()) + " ','FLISR_IN_OPERATION :The FLISR algorithm in the SAU is running');\n "

        try:
            cur.execute(SQLtext)
            conn.commit()
        #        	print(" trying to update flag FLISR_IN_OPERATION" )
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when writing DB')
            sys.exit(1)
        finally:
            print('successfully updated he flag FLISR_IN_OPERATION')
            # finally we close the connection
            conn.close()


def update_FLISR_failure_flag(is_FLISR_failed, conn_txt):
    if is_FLISR_failed == 1:
        SQLtext = "";
        # Try connection to DB
        try:
            # DB connection parameters, change those if needed
            conn = psycopg2.connect(conn_txt)
        #        	print("trying to connect to the database for updating flag FLISR_FAILURE ")
        except psycopg2.OperationalError as e:
            print('Unable to connect!\n{0}').format(e)
            sys.exit(1)
        else:
            #            print('Connected!')
            # we truncate (delete) the data already present in DB, so that we do not have overlappings.  If you do not want to do that delete the following line.
            # But consider that the indexes used for "Id" start always from 1, so we may have some double items and the DB will not like it as Id is defined as primary key
            # To avoid such problem, you can adapt the code by reading from the DB the biggest Id and to start from the following
            cur = conn.cursor()
            SQLtext += " INSERT INTO management.algorithm_log(algorithm_id,timestamp,log_message)  "
            SQLtext += " VALUES('4','" + str(
                datetime.utcnow()) + " ','FLISR_FAILURE : The FLISR algorithm failed and it is not able to publish the results');\n "

        try:
            cur.execute(SQLtext)
            conn.commit()
        #        	print(" trying to update flag FLISR_FAILURE" )
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when writing DB')
            sys.exit(1)
        finally:
            print('successfully updated he flag FLISR_FAILURE')
            # finally we close the connection
            conn.close()


def update_FLISR_publish_flag(is_FLISR_failed, is_FLISR_in_operation, conn_txt):
    if is_FLISR_failed == 0 and is_FLISR_in_operation == 0:
        SQLtext = "";
        # Try connection to DB
        try:
            # DB connection parameters, change those if needed
            conn = psycopg2.connect(conn_txt)
        #        	print("trying to connect to the database for updating flag FLISR_PUBLISHED ")
        except psycopg2.OperationalError as e:
            print('Unable to connect!\n{0}').format(e)
            sys.exit(1)
        else:
            #            print('Connected!')
            # we truncate (delete) the data already present in DB, so that we do not have overlappings.  If you do not want to do that delete the following line.
            # But consider that the indexes used for "Id" start always from 1, so we may have some double items and the DB will not like it as Id is defined as primary key
            # To avoid such problem, you can adapt the code by reading from the DB the biggest Id and to start from the following
            cur = conn.cursor()
            SQLtext += " INSERT INTO management.algorithm_log(algorithm_id,timestamp,log_message) "
            SQLtext += " VALUES('4','" + str(
                datetime.utcnow()) + " ','FLISR_PUBLISHED : The FLISR algorithm has finished and the output is in the SQL table, waiting for the response of the devices');\n "

        try:
            cur.execute(SQLtext)
            conn.commit()
        #        	print(" trying to update flag FLISR_PUBLISHED" )
        except psycopg2.OperationalError as e:
            print('Unable to execute query!\n{0}').format(e)
            print('some issues when writing DB')
            sys.exit(1)
        finally:
            print('successfully updated he flag FLISR_PUBLISHED')
            # finally we close the connection
            conn.close()


def update_global_FLISR_successful_flag(conn_txt):
    SQLtext = "";
    try:
        # DB connection parameters, change those if needed
        conn = psycopg2.connect(conn_txt)
    #        print("trying to connect to the database for updating flag: global_FLISR_successful_flag : PR_SF ")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)
    else:
        #            print('Connected!')
        # we truncate (delete) the data already present in DB, so that we do not have overlappings.  If you do not want to do that delete the following line.
        # But consider that the indexes used for "Id" start always from 1, so we may have some double items and the DB will not like it as Id is defined as primary key
        # To avoid such problem, you can adapt the code by reading from the DB the biggest Id and to start from the following
        cur = conn.cursor()
        SQLtext += " INSERT INTO management.flag(ftype_id,flag_timestamp,flag_message) "
        SQLtext += " VALUES(9,'" + str(
            datetime.utcnow()) + " ','Power Restoration  Successfully Finished.Message for State Estimation and Power Control  ');\n "

    try:
        cur.execute(SQLtext)
        conn.commit()
    #        print(" trying to update flag : global_FLISR_successful_flag : PR_SF" )
    except psycopg2.OperationalError as e:
        print('Unable to execute query!\n{0}').format(e)
        print('some issues when writing DB')
        sys.exit(1)
    finally:
        print('successfully updated the flag :global_FLISR_successful_flag : PR_SF')
        # finally we close the connection
        conn.close()


def update_global_FLISR_unsuccessful_flag(conn_txt):
    SQLtext = "";
    try:
        # DB connection parameters, change those if needed
        conn = psycopg2.connect(conn_txt)
    #        print("trying to connect to the database for updating flag : global_FLISR_unsuccessful_flag : PR_NSF  ")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)
    else:
        #            print('Connected!')
        # we truncate (delete) the data already present in DB, so that we do not have overlappings.  If you do not want to do that delete the following line.
        # But consider that the indexes used for "Id" start always from 1, so we may have some double items and the DB will not like it as Id is defined as primary key
        # To avoid such problem, you can adapt the code by reading from the DB the biggest Id and to start from the following
        cur = conn.cursor()
        SQLtext += " INSERT INTO management.flag(ftype_id,flag_timestamp,flag_message) "
        SQLtext += " VALUES(10,'" + str(
            datetime.utcnow()) + " ','Power Restoration Not Successfully Finished.Message for State Estimation and Power Control  ');\n "

    try:
        cur.execute(SQLtext)
        conn.commit()
    #        print(" trying to update flag : global_FLISR_unsuccessful_flag : PR_NSF" )
    except psycopg2.OperationalError as e:
        print('Unable to execute query!\n{0}').format(e)
        print('some issues when writing DB')
        sys.exit(1)
    finally:
        print('successfully updated the flag : global_FLISR_unsuccessful_flag : PR_NSF')
        # finally we close the connection
        conn.close()


def update_global_FLISR_IED_comm_fail_flag(conn_txt):
    SQLtext = "";
    try:
        # DB connection parameters, change those if needed
        conn = psycopg2.connect(conn_txt)
    #        print("trying to connect to the database for updating flag : global_FLISR_IED_comm_fail_flag : PR_IED_CE ")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)
    else:
        #            print('Connected!')
        # we truncate (delete) the data already present in DB, so that we do not have overlappings.  If you do not want to do that delete the following line.
        # But consider that the indexes used for "Id" start always from 1, so we may have some double items and the DB will not like it as Id is defined as primary key
        # To avoid such problem, you can adapt the code by reading from the DB the biggest Id and to start from the following
        cur = conn.cursor()
        SQLtext += " INSERT INTO management.flag(ftype_id,flag_timestamp,flag_message) "
        SQLtext += " VALUES(11,'" + str(
            datetime.utcnow()) + " ','Power Restoration.Communication Error with IED.  ');\n "

    try:
        cur.execute(SQLtext)
        conn.commit()
    #        print(" trying to update flag : global_FLISR_IED_comm_fail_flag : PR_IED_CE" )
    except psycopg2.OperationalError as e:
        print('Unable to execute query!\n{0}').format(e)
        print('some issues when writing DB')
        sys.exit(1)
    finally:
        print('successfully updated he flag : global_FLISR_IED_comm_fail_flag : PR_IED_CE')
        # finally we close the connection
        conn.close()


def update_global_FLISR_start_flag(conn_txt):
    SQLtext = "";
    try:
        # DB connection parameters, change those if needed
        conn = psycopg2.connect(conn_txt)
    #        print("trying to connect to the database for updating flag : global_FLISR_start_flag : PR_STRT ")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
        sys.exit(1)
    else:
        #            print('Connected!')
        # we truncate (delete) the data already present in DB, so that we do not have overlappings.  If you do not want to do that delete the following line.
        # But consider that the indexes used for "Id" start always from 1, so we may have some double items and the DB will not like it as Id is defined as primary key
        # To avoid such problem, you can adapt the code by reading from the DB the biggest Id and to start from the following
        cur = conn.cursor()
        SQLtext += " INSERT INTO management.flag(ftype_id,flag_timestamp,flag_message) "
        SQLtext += " VALUES(12,'" + str(
            datetime.utcnow()) + " ','Power Restoration. The Power Restoration algorithm started and running.  ');\n "

    try:
        cur.execute(SQLtext)
        conn.commit()
    #        print(" trying to update flag : global_FLISR_start_flag : PR_STRT" )
    except psycopg2.OperationalError as e:
        print('Unable to execute query!\n{0}').format(e)
        print('some issues when writing DB')
        sys.exit(1)
    finally:
        print('successfully updated he flag : global_FLISR_start_flag : PR_STRT')
        # finally we close the connection
        conn.close()

#detect fault function:

def detect_fault_Rpi(conn_txt, real_status_excel):
    #########################  retrieve the latest  breaker statuses from the excel sheet#######

    switches_excel = pd.read_excel(real_status_excel, sheetname='switches')
    name = list(switches_excel['name'])
    list_excel_status = list(switches_excel['status'])
    list_excel_tripped = list(switches_excel['switch_tripped'])

    #########################  retrieve the latest stored breaker statuses from the datbase #######
    switches_database = fetch_switches_flisr_table(conn_txt)
    is_fault_detected = 0
    n = 0

    ################################## update the switch data to be used by the algorithm ###########################
    for i, elrow in switches_database.iterrows():
        if list_excel_tripped[n] == 0:  # if there is at least a CB that tripped, the FLISR is started
            is_fault_detected = 1

        if list_excel_status[n] == -1:
            switches_database.loc[switches_database['name'] == name[n], 'status'] = 0
            switches_database.loc[switches_database['name'] == name[n], 'switch_tripped'] = list_excel_tripped[n]
        else:
            switches_database.loc[switches_database['name'] == name[n], 'status'] = list_excel_status[n]
            switches_database.loc[switches_database['name'] == name[n], 'switch_tripped'] = list_excel_tripped[n]
        n = n + 1
    print('updating the database with the latest breaker statuses')
    print(switches_database)
    update_tripped_switches_coloumn(conn_txt, switches_database)
    update_switches_status_coloumn(conn_txt, switches_database)
    return is_fault_detected, switches_database


########### DEFINE CLASSES IN ORDER TO INITIALIZE STATE ESTIMATOR MEASUREMENTS #########

class dssedata(object):
    def __init__(self, nstate=0, numnr=20, time=1):
        self.nstate = nstate
        self.numnr = numnr
        self.time = time
        self.dsseinstance = []

    def adddsseinstance(self, estimationid, mtype, phase, itemid1, itemid2, estimationaccuracy, estimationvalue):
        self.dsseinstance.append(
            dsseinstance(estimationid, mtype, phase, itemid1, itemid2, estimationaccuracy, estimationvalue))


class dsseinstance(object):
    def __init__(self, estimationid, mtype, phase, itemid1, itemid2, estimationaccuracy, estimationvalue):
        self.estimationid = estimationid
        self.mtype = mtype
        self.phase = phase
        self.itemid1 = itemid1
        self.itemid2 = itemid2
        self.estimationaccuracy = estimationaccuracy
        self.estimationvalue = estimationvalue

################## definition of class objects for generating the measurement data for SE ##########################
class measdata(object):
    def __init__(self, nmeas=0, time=0, switchphmeas=0):
        self.nmeas = nmeas
        self.time = time
        self.switchphmeas = switchphmeas
        self.measurementinstance = []

    def addmeasurementinstance(self, measurementid, mtype, phase, itemid, measurementaccuracy, measurementvalue):
        self.measurementinstance.append(
            measurementinstance(measurementid, mtype, phase, itemid, measurementaccuracy, measurementvalue))

class measurementinstance(object):
    def __init__(self, measurementid, mtype, phase, itemid, measurementaccuracy, measurementvalue):
        self.measurementid = measurementid
        self.mtype = mtype
        self.phase = phase
        self.itemid = itemid
        self.measurementaccuracy = measurementaccuracy
        self.measurementvalue = measurementvalue

class griddata(object):
    def __init__(self, pbase=1000000, gtype=1, nnode=0, nline=0, nswitch=0, vbasenom=8660):
        self.pbase = pbase
        self.vbasenom = vbasenom
        self.nline = nline
        self.nnode = nnode
        self.nswitch = nswitch
        self.gtype = gtype
        self.node = []
        self.line = []

    def addnode(self, nodeid, vbase):
        self.node.append(node(nodeid, vbase))

    def addline(self, lineid, phasea, phaseb, phasec, linefrom, lineto, amax):
        self.line.append(line(lineid, phasea, phaseb, phasec, linefrom, lineto, amax))

    def addswitch(self, switchid, nodefrom, nodeto, switchtripped, status, amax):
        self.node.append(switch(switchid, nodefrom, nodeto, switchtripped, status, amax))

class node(object):
    def __init__(self, nodeid, vbase=8660):
        self.nodeid = nodeid
        self.vbase = vbase

class switch(object):
    def __init__(self, switchid, nodefrom, nodeto, switchtripped, status, amax):
        self.switchid = switchid
        self.nodefrom = nodefrom
        self.nodeto = nodeto
        self.switchtripped = switchtripped
        self.status = status
        self.amax = amax

class line(object):
    def __init__(self, lineid, phasea, phaseb, phasec, linefrom, lineto, amax):
        self.lineid = lineid
        self.phasea = phasea
        self.phaseb = phaseb
        self.phasec = phasec
        self.linefrom = linefrom
        self.lineto = lineto
        self.amax = amax
        self.phaseP11 = []
        self.phaseP12 = []
        self.phaseP13 = []
        self.phaseP22 = []
        self.phaseP23 = []
        self.phaseP33 = []

    def addphaseP11(self, resistance, reactance, susceptance, conductance):
        self.phaseP11.append(phaseP11(resistance, reactance, susceptance, conductance))

    def addphaseP12(self, resistance, reactance, susceptance, conductance):
        self.phaseP12.append(phaseP12(resistance, reactance, susceptance, conductance))

    def addphaseP13(self, resistance, reactance, susceptance, conductance):
        self.phaseP13.append(phaseP13(resistance, reactance, susceptance, conductance))

    def addphaseP22(self, resistance, reactance, susceptance, conductance):
        self.phaseP22.append(phaseP22(resistance, reactance, susceptance, conductance))

    def addphaseP23(self, resistance, reactance, susceptance, conductance):
        self.phaseP23.append(phaseP23(resistance, reactance, susceptance, conductance))

    def addphaseP33(self, resistance, reactance, susceptance, conductance):
        self.phaseP33.append(phaseP33(resistance, reactance, susceptance, conductance))

class phaseP11(object):
    def __init__(self, resistance, reactance, susceptance, conductance):
        self.resistance = resistance
        self.reactance = reactance
        self.susceptance = susceptance
        self.conductance = conductance

class phaseP12(object):
    def __init__(self, resistance, reactance, susceptance, conductance):
        self.resistance = resistance
        self.reactance = reactance
        self.susceptance = susceptance
        self.conductance = conductance

class phaseP13(object):
    def __init__(self, resistance, reactance, susceptance, conductance):
        self.resistance = resistance
        self.reactance = reactance
        self.susceptance = susceptance
        self.conductance = conductance

class phaseP22(object):
    def __init__(self, resistance, reactance, susceptance, conductance):
        self.resistance = resistance
        self.reactance = reactance
        self.susceptance = susceptance
        self.conductance = conductance

class phaseP23(object):
    def __init__(self, resistance, reactance, susceptance, conductance):
        self.resistance = resistance
        self.reactance = reactance
        self.susceptance = susceptance
        self.conductance = conductance

class phaseP33(object):
    def __init__(self, resistance, reactance, susceptance, conductance):
        self.resistance = resistance
        self.reactance = reactance
        self.susceptance = susceptance
        self.conductance = conductance

class databaseconn(object):
	def __init__(self,DBname,user,password,ipaddress,port):
		self.DBname = DBname
		self.user = user
		self.password = password
		self.ipaddress = ipaddress
		self.port = port

## basic db parameters
dbname = 'SAU'
user = 'postgres'
host = '134.130.169.25'
password = 'postgres'
port = '5432'

### creating the connection string to be parsed into the db_functions call back functions #############################
conn_txt = "";
conn_txt += "dbname="
conn_txt += dbname;
conn_txt += " user="
conn_txt += user;
conn_txt += " host="
conn_txt += host;
conn_txt += " password="
conn_txt += password;

###################################### Create empty graph#####################################################
grid_lines=net.Graph()
grid_switches=net.Graph()
grid_loads=net.Graph()
grid_nodes=net.Graph()

#opening n excel file to write the elapsed time for the  computation
workbook = xlsxwriter.Workbook('time_SR_use_case_2.xlsx')
worksheet = workbook.add_worksheet()
row=0

#iterating the process i-times (to check elapsed time statistics)
for i in range(1):

    #counter for the selection of cloumn and row of the excel file with elapsed time
    xyz = 1

    #### WEIGHTING FACTORS TO BE DEFINED!!!!!
    u1=500
    u2=1
    u3=1
    u4=1

    #create conn cursor for state estimation
    databaseconn1 = databaseconn(dbname,user,password,host,port) #class for DB connection
    conn_SE = connDB(databaseconn1) #function for connection to DB

    n=0
    directory=os.path.dirname(os.path.abspath(__file__))
    file_name='/Networkdata_MV_FLISR_use_case_1.xlsx'
    real_status_excel=directory+file_name

    ############################### update the database ##############################
    switches_excel = pd.read_excel(real_status_excel,sheetname='switches')
    lines_excel = pd.read_excel(real_status_excel,sheetname='lines')
    loads_excel = pd.read_excel(real_status_excel,sheetname='loads')
    nodes_excel = pd.read_excel(real_status_excel,sheetname='nodes')
    ltype_excel = pd.read_excel(real_status_excel,sheetname='ltype')
    general = pd.read_excel(real_status_excel,sheetname='general')

    upd_tm_lines=update_line_flisr_table(conn_txt,lines_excel,1)
    upd_tm_switches=update_switches_flisr_table(conn_txt,switches_excel,1)
    upd_tm_loads=update_loads_flisr_table(conn_txt,loads_excel,1)
    upd_tm_nodes=update_nodes_flisr_table(conn_txt,nodes_excel,1)
    upd_tm_ltype=update_ltype_flisr_table(conn_txt,ltype_excel,1)

    ################################# detect in real time if there was a fault ##########################################
    tstart=time.time() #start measuring time
    is_fault_detected=detect_fault_Rpi(conn_txt,real_status_excel)

    #lines_updated=fetch_lines_flisr_table(conn_txt)
    #loads_updated=fetch_loads_flisr_table(conn_txt)
    #nodes_updated=fetch_nodes_flisr_table(conn_txt)

        ################## if fault detected update flag and start the FLISR ##########################################
    if is_fault_detected[0]==1:
        update_fault_detection_flag(is_fault_detected,conn_txt) # update fault detection flag

        all_possible_loads=0
        while all_possible_loads==0:

            is_FLISR_in_operation =1
            to_be_closed, subgrid, set_SS, switches_updated, lines_updated, loads_updated, nodes_updated, all_possible_loads = flisr_algorithm(conn_SE,conn_txt,is_fault_detected, is_FLISR_in_operation, general, u1, u2, u3, u4)
            if all_possible_loads==0:
                for i, elrow in switches_updated.iterrows():
                ################ update the switches status (close the corresponding tie-unit) in the DB ###################
                    for j in range(len(to_be_closed)):
                        switches_updated.loc[switches_updated['name'] == to_be_closed[j], 'status'] = 1
                print(switches_updated)
                upd_tm_switches=update_switches_status_coloumn(conn_txt,switches_updated)

                ############### update the SS reference for each node (if a new connection exists) in the DB #################

                grid_nodes=net.Graph()
                for i, elrow in nodes_updated.iterrows():
                    grid_nodes.add_node(int(elrow[0]), attr_dict=elrow[1:].to_dict())
                for i in set_SS:
                    for (p, d) in grid_nodes.nodes(data=True):
                        if (net.has_path(subgrid, p, i)):
                            nodes_updated.loc[nodes_updated['nodeid'] == p, 'reference_ss_node'] = i
                upd_tm_nodes=update_node_reference_coloumn(conn_txt,nodes_updated)

            tend = time.time()
            ttot=tend - tstart
            print('ELAPSED TIME:', ttot) #measured time for each DB update

            #manually set the second, consequent fault, in node M1, these switches trip:
            switches_updated.loc[switches_updated['name'] == 'N1_M4', 'status'] = 0
            switches_updated.loc[switches_updated['name'] == 'N1_M4', 'switch_tripped'] = 0
            switches_updated.loc[switches_updated['name'] == 'M1_L3', 'status'] = 0
            switches_updated.loc[switches_updated['name'] == 'M1_L3', 'switch_tripped'] = 0
            switches_updated.loc[switches_updated['name'] == 'F1_M1', 'status'] = 0
            switches_updated.loc[switches_updated['name'] == 'F1_M1', 'switch_tripped'] = 0
            upd_tm_switches = update_switches_status_coloumn(conn_txt, switches_updated)
            upd_tm_switches = update_tripped_switches_coloumn(conn_txt, switches_updated)

            if xyz ==1:
                worksheet.write(row, 0, ttot)
                xyz += 1
            elif xyz ==2:
                worksheet.write(row, 1, ttot)
                xyz += 1
                #row+=1
            elif xyz == 3:
                worksheet.write(row, 2, ttot)
                xyz += 1
                row+=1

        update_global_FLISR_successful_flag(conn_txt)


    else:
        print('no fault identified')

workbook.close()
