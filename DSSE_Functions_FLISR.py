# coding=utf8
###############################################################################
# DSSE Functions for FLISR
#
# @authors Andrea Angioni
# @copyright 2016-2017, Institute for Automation of Complex Power Systems, RWTH Aachen University
# @license GNU General Public License (version 3)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import psycopg2, time, datetime
import random
import numpy
import cmath
import math
from datetime import datetime, timedelta
import sys, getopt

def testDSSE():
	nameDB = "Cloud_DB"
	userDB = "aan-iko"
	passDB = "password"
	IPDB = "134.130.169.25"
	portDB = "5432"
	groupgrid = "1"
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hn:u:p:i:g:", ["nameDB=", "userDB=", "passDB=", "IPDB=", "groupgrid="])
	except getopt.GetoptError:
		print("not recognized input, please type: \n function.py -n <nameDB> -u <userDB> -p <passDB> -i <IP DB> -g <groupgrid>")
		sys.exit(2)
	for opt, arg in opts:
		if opt == "-h":
			print("please type: \n function.py -n <nameDB> -u <userDB> -p <passDB> -i <IP DB>")
			sys.exit()
		elif opt in ("-n", "--nameDB"):
			nameDB = arg
			print("name of DB is "+nameDB+"")
		elif opt in ("-u", "--userDB"):
			userDB = arg
			print("user name for DB is "+userDB+"")
		elif opt in ("-p", "--passDB"):
			passDB = arg
			print("pasword for DB is *not printed*")
		elif opt in ("-i", "--IPDB"):
			print("IP address for DB is "+IPDB+"")
			IPDB = arg
		elif opt in ("-g", "--groupgrid"):
			print("Group grid is "+groupgrid+"")
			groupgrid = arg

	#print("The input parameters are: name of DB "+nameDB+", user name "+userDB+", pasword "+passDB+", IP address "+IPDB+", group_grid "+groupgrid+"")
	databaseconn1 = databaseconn(nameDB,userDB,passDB,IPDB,portDB) #class for DB connection
	conn = connDB(databaseconn1) #function for connection to DB
	#griddata1 = readGridData(conn,groupgrid) #function to read grid data from DB table and rearrange in classes for python
	griddata1 = readGridDataFLISR(conn) #function to read grid data, slightly adapted for FLISR
	setdsse1=initDSSE(conn)
	while True:
		ttot0=0;
		measdata1 = readMeasData(conn,setdsse1,griddata1,groupgrid) #function to retrieve measurements from DB
		dssedata1 = DSSEVRI1 (griddata1,measdata1) #function with the actual state estimator, requires grid data and measurements as input
		ttot3 = writeDB(conn,dssedata1)
		print("time to read measurements "+str(measdata1.time)+" s;\ntime to run DSSE "+str(dssedata1.time)+" s;\ntime to write into DB "+str(ttot3)+" s")
		if (setdsse1.treal - dssedata1.time - measdata1.time -ttot3)>0:
			time.sleep(setdsse1.treal - dssedata1.time - measdata1.time -ttot3) # it waits a time equal to the difference between the real time object and the time required b the DSSE, this should allow to run in real time every second
			if setdsse1.treal > setdsse1.trealset and( dssedata1.time + measdata1.time + ttot3) < float(setdsse1.treal/2): #if the real time target was changed but we have again very short computation time we bring it back to the original
				setdsse1.treal=math.ceil( dssedata1.time + measdata1.time + ttot3)
				print("elapsed time is "+str(dssedata1.time + measdata1.time + ttot3)+", real time target is again "+ str(setdsse1.treal) +".\n")
				SQLtext0=""
				SQLtext0+="INSERT INTO management.algorithm_log(algorithm_id,timestamp,log_message) "
				SQLtext0 += " VALUES('1','"+str(datetime.utcnow())+"','DSSE - CHANGE REAL TIME OBJECT TO "+ str(setdsse1.treal) +"');\n"
				SQLtext0 += "UPDATE management.algorithm_parameter SET parameter_value ="+ str(setdsse1.treal) +" WHERE  "
				SQLtext0 += "'parameter_name' ='DSSE_T_REAL_TIME' "
		else:
			setdsse1.treal=math.ceil( dssedata1.time + measdata1.time + ttot3)
			print("elapsed time is "+str(dssedata1.time + measdata1.time + ttot3)+", real time target readapted to "+ str(setdsse1.treal) +".\n")
			SQLtext0=""
			SQLtext0+="INSERT INTO management.algorithm_log(algorithm_id,timestamp,log_message) "
			SQLtext0 += " VALUES('1','"+str(datetime.utcnow())+"','DSSE - CHANGE REAL TIME OBJECT TO "+ str(setdsse1.treal) +"');\n"
			SQLtext0 += "UPDATE management.algorithm_parameter SET parameter_value ="+ str(setdsse1.treal) +" WHERE  "
			SQLtext0 += "'parameter_name' ='DSSE_T_REAL_TIME' "
			cur = conn.cursor()
			cur.execute(SQLtext0)
			conn.commit()
			#print("elapsed time is "+str(dssedata1.time + measdata1.time + ttot3)+"")

def readPowerData(conn,setdsse,griddata,groupgrid): #function to read power measruements, particularly needed by FLISR
	tlarge=60; #if there are few measurements we extend the search in the last minute
	start2=datetime.utcnow()
	print("current time: "+str(start2)+"")
	cur = conn.cursor()
	measdata1 = measdata()
	#1st part. we read the measurements that were updated before the last execution
	Nmeas = 0;
	ref_angle = 0;
	mtype1=['9','10','11','12']
	for index1,row1 in enumerate(mtype1):
		mtype=mtype1[index1]
		SQLtext = selectSQL(mtype,str(datetime.utcnow() - timedelta(seconds=setdsse.treal) ),str(datetime.utcnow()),groupgrid)
		#print(SQLtext)
		try:
			cur.execute(SQLtext)
			conn.commit()
			rows = cur.fetchall()
			for index,row in enumerate(rows):
				acc1 = float(row[3]);
				if   row[1]=='AN':
					ph1=1
				elif row[1]=='BN':
					ph1=2
				elif row[1]=='CN':
					ph1=3
				if int(mtype)==9 or int(mtype)==10 or int(mtype)==11 or int(mtype)==12 or int(mtype)==13 :
					measdata1.addmeasurementinstance(Nmeas,row[4],ph1,int(row[5]),acc1,row[2]/griddata.pbase) #index represents the itemid for both nodes and voltages, both of them starts from zero
				Nmeas=Nmeas+1;
		except psycopg2.OperationalError as e:
			print('Unable to execute query!\n{0}').format(e)
			sys.exit(1)
	measdata1.nmeas = Nmeas #this indicates the total number of measurements available
	#2nd part. we verify if we have enough measurements, if not we look more in the past
	if (griddata.nnode/(measdata1.nmeas+1)) > setdsse.xmeasmin: #if there are few measurements we extend the search in the last minute
		print("Not enough measurements in real time, looking back at "+str(setdsse.treal*setdsse.xmeaspast)+" seconds")
		SQLtext0=""
		SQLtext0+="INSERT INTO management.flag(ftype_id,flag_timestamp,flag_message) "
		SQLtext0 += " VALUES((SELECT ftype_id FROM management.flag_index WHERE ftype_name = 'SE_FM'),'"+str(datetime.utcnow())+"','FEW measurements available for DSSE');\n"
		cur.execute(SQLtext0)
		conn.commit()
		measdata1 = measdata()
		Nmeas=0;
		for index1,row1 in enumerate(mtype1):
			mtype=mtype1[index1]
			SQLtext = selectSQL(mtype,str(datetime.utcnow() - timedelta(seconds=setdsse.xmeaspast) ),str(datetime.utcnow()),groupgrid)
			try:
				cur.execute(SQLtext)
				conn.commit()
				rows = cur.fetchall()
				for index,row in enumerate(rows):
					acc1=float(row[3]);
					if row[1]=='AN':
						ph1=1
					elif row[1]=='BN':
						ph1=2
					elif row[1]=='CN':
						ph1=3
					if int(mtype)==9 or int(mtype)==10 or int(mtype)==11 or int(mtype)==12 or int(mtype)==13 :
						measdata1.addmeasurementinstance(Nmeas,row[4],ph1,int(row[5]),acc1,row[2]/griddata.pbase) #index represents the itemid for both nodes and voltages, both of them starts from zero
					Nmeas=Nmeas+1;
			except psycopg2.OperationalError as e:
				print('Unable to execute query!\n{0}').format(e)
				sys.exit(1)
		measdata1.nmeas=Nmeas
	print("number of real measurements is "+str(Nmeas)+"\n")
	#3rd part. we also add the pseudo measurements from the DB
	profile_id=['1','2']#such profile ids, can be chosen depending on which type of profile is needed
	SQLtext = ""
	SQLtext += "SELECT value,accuracy FROM measurecommand.pseudo_measurements_values WHERE (profile_id="+profile_id[0]+" OR profile_id="+profile_id[1]+") "
	SQLtext += "AND (60*extract(HOUR from timestamp '"+str(datetime.utcnow())+"')+extract(MINUTE from timestamp '"+str(datetime.utcnow())+"')) > (60*extract(HOUR from  timestamp1)+extract(MINUTE from  timestamp1))"
	SQLtext += "AND (60*extract(HOUR from timestamp '"+str(datetime.utcnow())+"')+extract(MINUTE from timestamp '"+str(datetime.utcnow())+"')) < (60*extract(HOUR from  timestamp2)+extract(MINUTE from  timestamp2)) ORDER BY profile_id ASC"
	cur.execute(SQLtext)
	conn.commit()
	rows1 = cur.fetchall()
	ph1=1 # for the moment just on phase AN
	if cur.rowcount > 0:
		Lpseudo_value=float(rows1[0][0])#pseudo values for loads
		Lpseudo_acc=float(rows1[0][1])
		Gpseudo_value=float(rows1[1][0])#pseudo values for generators
		Gpseudo_acc=float(rows1[1][1])
		for x in range(griddata.nnode):
			SQLtext = "SELECT nominals FROM networktopology.nodes RIGHT JOIN networktopology.injections ON networktopology.injections.node_id = networktopology.nodes.node_id WHERE (injection_type = '1' or injection_type = '2') AND networktopology.nodes.node_id = '"+str(griddata.node[x].nodeid)+"';"
			cur.execute(SQLtext)
			conn.commit()
			rows1 = cur.fetchall()
			SQLtext = "SELECT nominals FROM networktopology.nodes RIGHT JOIN networktopology.injections ON networktopology.injections.node_id = networktopology.nodes.node_id WHERE injection_type = '3' AND networktopology.nodes.node_id = '"+str(griddata.node[x].nodeid)+"' ;"
			cur.execute(SQLtext)
			conn.commit()
			rows2 = cur.fetchall()
			PLoadtot=0.0
			PGentot=0.0
			for row in rows1:
				PLoadtot=PLoadtot+0.333*Lpseudo_value*float(row[0])
			for row in rows2:
				PGentot=PGentot+0.333*Gpseudo_value*float(row[0])
			if x > 0:
				measdata1.addmeasurementinstance(Nmeas,9,ph1,griddata.node[x].nodeid,numpy.sqrt(Lpseudo_acc**2 + Gpseudo_acc**2),(PLoadtot+PGentot)*0.97/griddata.pbase)
				measdata1.addmeasurementinstance(Nmeas+1,11,ph1,griddata.node[x].nodeid,numpy.sqrt(Lpseudo_acc**2 + Gpseudo_acc**2),PLoadtot*0.2431/griddata.pbase) #reactive power of load assuming cosfi=0.97, for the generator we assume reactive power equal to zero
				Nmeas=Nmeas+2;
	measdata1.nmeas=Nmeas #this indicates the total number of measurements available
	print("number of real + pseudo measurements is "+str(Nmeas)+"\n")
	ttot2=datetime.utcnow()-start2
	measdata1.time=ttot2.total_seconds()
	#for x in range(measdata1.nmeas):
	#	print("measurement type = ",measdata1.measurementinstance[x].mtype,"; item_id = ",measdata1.measurementinstance[x].itemid,"; accuracy is = ",measdata1.measurementinstance[x].measurementaccuracy, "; value = ",measdata1.measurementinstance[x].measurementvalue)
	return measdata1

def readMeasData(conn,setdsse,griddata,groupgrid): #function to read measruement data and rearrange in measdata class
	tlarge=60; #if there are few measurements we extend the search in the last minute
	start2=datetime.utcnow()
	print("current time: "+str(start2)+"")

	cur = conn.cursor()
	measdata1 = measdata()
	#1st part. we read the measurements that were updated before the last execution
	Nmeas = 0;
	ref_angle = 0;
	mtype1=['1','3','5','7','9','11','13','15']
	for index1,row1 in enumerate(mtype1):
		mtype=mtype1[index1]
		#SQLtext = selectSQL(mtype,str(datetime.utcnow() - timedelta(seconds=setdsse.treal) ),str(datetime.utcnow()),groupgrid)
		SQLtext = selectSQLFLISR(mtype,str(datetime.utcnow() - timedelta(seconds=setdsse.treal) ),str(datetime.utcnow()))
		#print(SQLtext)
		try:
			cur.execute(SQLtext)
			conn.commit()
			rows = cur.fetchall()
			for index,row in enumerate(rows):
				acc1 = float(row[3]);
				if   row[1]=='AN':
					ph1=1
				elif row[1]=='BN':
					ph1=2
				elif row[1]=='CN':
					ph1=3
				if int(mtype)==1: #V magnitude single phase
					measdata1.addmeasurementinstance(Nmeas,row[4],ph1,int(row[5]),acc1,row[2]/( griddata.node[int(row[5])].vbase)) #index represents the itemid for both nodes and voltages, both of them starts from zero
				elif int(mtype)==3: #V phase single phase
					if int(row[5]) == 0:
						ref_angle = row[2]; # reference node identified
					measdata1.addmeasurementinstance(Nmeas,row[4],ph1,int(row[5]),acc1,row[2] - ref_angle)
				elif int(mtype)==7: #I phase single phase
					measdata1.addmeasurementinstance(Nmeas,row[4],ph1,int(row[5]),acc1,row[2] - ref_angle)
				elif int(mtype)==5: #I magnitude single phase
					measdata1.addmeasurementinstance(Nmeas,row[4],ph1,int(row[5]),acc1,row[2]* (griddata.node[griddata.line[int(row[5])].linefrom].vbase) / griddata.pbase) #index represents the itemid for both nodes and voltages, both of them starts from zero
				elif int(mtype)==9 or int(mtype)==11 or int(mtype)==13 or int(mtype)==15:
					measdata1.addmeasurementinstance(Nmeas,row[4],ph1,int(row[5]),acc1,row[2]/griddata.pbase) #index represents the itemid for both nodes and voltages, both of them starts from zero
				else:
					measdata1.addmeasurementinstance(Nmeas,row[4],ph1,int(row[5]),acc1,row[2]) #index represents the itemid for both nodes and voltages, both of them starts from zero
				#measurementinstance(measurementid,mtype,phase,itemid,measurementaccuracy,measurementvalue):
				Nmeas=Nmeas+1;
		except psycopg2.OperationalError as e:
			print('Unable to execute query!\n{0}').format(e)
			sys.exit(1)
	measdata1.nmeas = Nmeas #this indicates the total number of measurements available
	#2nd part. we verify if we have enough measurements, if not we look more in the past
	if (griddata.nnode/(measdata1.nmeas+1)) > setdsse.xmeasmin: #if there are few measurements we extend the search in the last minute
		print("Not enough measurements in real time, looking back at "+str(setdsse.treal*setdsse.xmeaspast)+" seconds")
		SQLtext0=""
		SQLtext0+="INSERT INTO management.flag(ftype_id,flag_timestamp,flag_message) "
		SQLtext0 += " VALUES((SELECT ftype_id FROM management.flag_index WHERE ftype_name = 'SE_FM'),'"+str(datetime.utcnow())+"','FEW measurements available for DSSE');\n"
		cur.execute(SQLtext0)
		conn.commit()
		measdata1 = measdata()
		Nmeas=0;
		for index1,row1 in enumerate(mtype1):
			mtype=mtype1[index1]
			#SQLtext = selectSQL(mtype,str(datetime.utcnow() - timedelta(seconds=setdsse.xmeaspast) ),str(datetime.utcnow()),groupgrid)
			SQLtext = selectSQLFLISR(mtype,str(datetime.utcnow() - timedelta(seconds=setdsse.xmeaspast) ),str(datetime.utcnow()))
			try:
				cur.execute(SQLtext)
				conn.commit()
				rows = cur.fetchall()
				for index,row in enumerate(rows):
					acc1=float(row[3]);
					if row[1]=='AN':
						ph1=1
					elif row[1]=='BN':
						ph1=2
					elif row[1]=='CN':
						ph1=3
					if int(mtype)==1:
						measdata1.addmeasurementinstance(Nmeas,row[4],ph1,int(row[5]),acc1,row[2]/griddata.node[int(row[5])].vbase ) #index represents the itemid for both nodes and voltages, both of them starts from zero
					elif int(mtype)==3:
						if int(row[5]) == 0 :
							ref_angle = row[2]; # reference node identified
						measdata1.addmeasurementinstance(Nmeas,row[4],ph1,int(row[5]),acc1,row[2] - ref_angle)
					elif int(mtype)==7:
						measdata1.addmeasurementinstance(Nmeas,row[4],ph1,int(row[5]),acc1,row[2] - ref_angle)
					elif int(mtype)==5:
						measdata1.addmeasurementinstance(Nmeas,row[4],ph1,int(row[5]),acc1,row[2]* (griddata.node[griddata.line[int(row[5])].linefrom].vbase ) / griddata.pbase) #index represents the itemid for both nodes and voltages, both of them starts from zero
					elif int(mtype)==9 or int(mtype)==11 or int(mtype)==13 or int(mtype)==15:
						measdata1.addmeasurementinstance(Nmeas,row[4],ph1,int(row[5]),acc1,row[2]/griddata.pbase) #index represents the itemid for both nodes and voltages, both of them starts from zero
					else:
						measdata1.addmeasurementinstance(Nmeas,row[4],ph1,int(row[5]),acc1,row[2]) #index represents the itemid for both nodes and voltages, both of them starts from zero
					#measurementinstance(measurementid,mtype,phase,itemid,measurementaccuracy,measurementvalue):
					Nmeas=Nmeas+1;
			except psycopg2.OperationalError as e:
				print('Unable to execute query!\n{0}').format(e)
				sys.exit(1)
		measdata1.nmeas=Nmeas
	print("number of real measurements is "+str(Nmeas)+"\n")
	#3rd part. we also add the pseudo measurements from the DB
	profile_id=['1','2']#such profile ids, can be chosen depending on which type of profile is needed
	SQLtext = ""
	SQLtext += "SELECT value,accuracy FROM measurecommand.pseudo_measurements_values WHERE (profile_id="+profile_id[0]+" OR profile_id="+profile_id[1]+") "
	SQLtext += "AND (60*extract(HOUR from timestamp '"+str(datetime.utcnow())+"')+extract(MINUTE from timestamp '"+str(datetime.utcnow())+"')) > (60*extract(HOUR from  timestamp1)+extract(MINUTE from  timestamp1))"
	SQLtext += "AND (60*extract(HOUR from timestamp '"+str(datetime.utcnow())+"')+extract(MINUTE from timestamp '"+str(datetime.utcnow())+"')) < (60*extract(HOUR from  timestamp2)+extract(MINUTE from  timestamp2)) ORDER BY profile_id ASC"
	cur.execute(SQLtext)
	conn.commit()
	rows1 = cur.fetchall()
	ph1=1 # for the moment just on phase AN
	if cur.rowcount > 0:
		Lpseudo_value=float(rows1[0][0])#pseudo values for loads
		Lpseudo_acc=float(rows1[0][1])
		Gpseudo_value=float(rows1[1][0])#pseudo values for generators
		Gpseudo_acc=float(rows1[1][1])
		for x in range(griddata.nnode):
			SQLtext = "SELECT nominals FROM networktopology.nodes RIGHT JOIN networktopology.injections ON networktopology.injections.node_id = networktopology.nodes.node_id WHERE (injection_type = '1' or injection_type = '2') AND networktopology.nodes.node_id = '"+str(griddata.node[x].nodeid)+"';"
			cur.execute(SQLtext)
			conn.commit()
			rows1 = cur.fetchall()
			SQLtext = "SELECT nominals FROM networktopology.nodes RIGHT JOIN networktopology.injections ON networktopology.injections.node_id = networktopology.nodes.node_id WHERE injection_type = '3' AND networktopology.nodes.node_id = '"+str(griddata.node[x].nodeid)+"' ;"
			cur.execute(SQLtext)
			conn.commit()
			rows2 = cur.fetchall()
			PLoadtot=0.0
			PGentot=0.0
			for row in rows1:
				PLoadtot=PLoadtot+0.333*Lpseudo_value*float(row[0])
			for row in rows2:
				PGentot=PGentot+0.333*Gpseudo_value*float(row[0])
			if x > 0:
				measdata1.addmeasurementinstance(Nmeas,9,ph1,griddata.node[x].nodeid,numpy.sqrt(Lpseudo_acc**2 + Gpseudo_acc**2),(PLoadtot+PGentot)*0.97/griddata.pbase)
				measdata1.addmeasurementinstance(Nmeas+1,11,ph1,griddata.node[x].nodeid,numpy.sqrt(Lpseudo_acc**2 + Gpseudo_acc**2),PLoadtot*0.2431/griddata.pbase) #reactive power of load assuming cosfi=0.97, for the generator we assume reactive power equal to zero
				Nmeas=Nmeas+2;
	measdata1.nmeas=Nmeas #this indicates the total number of measurements available
	print("number of real + pseudo measurements is "+str(Nmeas)+"\n")
	#4th part. we insert some virtual measurements. Basically it is the knowledge that the voltage magnitude and phase angle are normally in a certain range (nominal + 20%)
	#This step will always make the system observable
	for x in range(griddata.nnode):
		measdata1.addmeasurementinstance(Nmeas,1,ph1,griddata.node[x].nodeid,0.2,1) #1 pu
		measdata1.addmeasurementinstance(Nmeas+1,3,ph1,griddata.node[x].nodeid,0.2,0) #0 radians
		Nmeas=Nmeas+2;
	measdata1.nmeas=Nmeas #this indicates the total number of measurements available
	print("number of total measurements is "+str(Nmeas)+"\n")
	ttot2=datetime.utcnow()-start2
	measdata1.time=ttot2.total_seconds()
	#for x in range(measdata1.nmeas):
	#	print("measurement type = ",measdata1.measurementinstance[x].mtype,"; item_id = ",measdata1.measurementinstance[x].itemid,"; accuracy is = ",measdata1.measurementinstance[x].measurementaccuracy, "; value = ",measdata1.measurementinstance[x].measurementvalue)
	return measdata1

def DSSEVRI1 (griddata,measdata):
	# Distribution System State Estimation function
	# requires grid data and measurements as input, it produces a class with the results of the estimation in output
	# it is a Weighted Least Square estimator based on rectangular coordinates of voltage phasor
	# measurements are converted eiher to voltage or current phasors. In this way the problem is linearized and the jacobian matrix
	# does not have to be calculated at each iteration of the Netwon Rapson method.
	start1 = datetime.now()
	wmatrix1 = weightmatrixVRI1(griddata,measdata) #function to calculate the weight matrix
	[jmatrix1,measdata] = jacobianmatrixVRI1(griddata,measdata) #function to calculate the Jacobian matrix
	HW = numpy.transpose(jmatrix1).dot(wmatrix1)
	G1 = HW.dot(jmatrix1); #Gain matrix
	[jmatrix2,measdata2] = jacobianmatrixIRI1(griddata,measdata) #function to calculate the Jacobian matrix
	HW2 = numpy.transpose(jmatrix2).dot(wmatrix1)
	G2 = HW2.dot(jmatrix2); #Gain matrix
	Vmagnstatus  = numpy.ones((griddata.nnode,1)) #initialize the voltage magnitude and phase angle
	Vphasestatus = numpy.zeros((griddata.nnode,1))
	Vreal = numpy.ones((griddata.nnode,1)) #similarly we initialize the voltage real and imaginary parts
	Vimag = numpy.zeros((griddata.nnode,1))
	Imagn = numpy.zeros((griddata.nline,1))
	Iphase = numpy.zeros((griddata.nline,1))
	Imagnload = numpy.zeros((griddata.nnode,1))
	Iphaseload = numpy.zeros((griddata.nnode,1))
	max_delta=10; #the maximum delta is used as criteria to stop the Netwon Rapson iteration. We do here a dummy inizialitazion
	iteration=0; #number of iterations of NR
	while max_delta > 0.0000001 and iteration < 50: #conditions to stop NR
		iteration=iteration+1;
		#print("iteration number "+str(iteration)+";")
		z = measurementvector1(griddata,measdata,Vreal,Vimag) #measurement vector (all measurements converted to voltages and currents)
		hx = hxvector1(griddata,measdata,Vreal,Vimag)#vector with measurement quantity calculated based on the estimated state at the ith iteration
		res = z - hx;#residual vector (to be minimized)
		HWres = HW.dot(res)
		delta = numpy.linalg.solve(G1,HWres) #the delta vector is calculated
		if measdata.switchphmeas==0:
			#jmatrix=numpy.delete(jmatrix, 1, 1)
			Vreal[0,0]=Vreal[0,0]+delta[0,0] #it is summed to the current state. The imaginary part at the slack is by assumption always zero
			Vmagnstatus[0,0]=abs(Vreal[0,0]+1j*Vimag[0,0])
			n=0;
			for x in range(1, griddata.nnode): #then we keep on summing delta to the other elements of the states
				n=n+1;
				Vreal[x,0]=Vreal[x,0]+delta[n,0]
				n=n+1;
				Vimag[x,0]=Vimag[x,0]+delta[n,0]
				Vmagnstatus[x,0]=abs(Vreal[x,0]+1j*Vimag[x,0])
				Vphasestatus[x,0]=cmath.phase(Vreal[x,0]+1j*Vimag[x,0])
		else:
			n=0;
			for x in range(0, griddata.nnode): #then we keep on summing delta to the other elements of the states
				Vreal[x,0]=Vreal[x,0]+delta[n,0]
				n=n+1;
				Vimag[x,0]=Vimag[x,0]+delta[n,0]
				n=n+1;
				Vmagnstatus[x,0]=abs(Vreal[x,0]+1j*Vimag[x,0])
				Vphasestatus[x,0]=cmath.phase(Vreal[x,0]+1j*Vimag[x,0])
		max_delta=max(abs(delta)) #we calculated the maximum element of delta
	# print("Total number of iterations is " + str(iteration) +"")
	# print("voltage magnitude is")
	# print(Vmagnstatus)
	# print("voltage phase angle is")
	# print(Vphasestatus)
	uncertaintytateV = calculatecovestimationV(G1,griddata,Vmagnstatus,Vphasestatus,measdata)#we calculate the estimated uncertainty of the state
	(Ibranchireal,Ibranchiimag,Ibranchfreal,Ibranchfimag,Iloadreal,Iloadimag)=calccurrentsI(griddata,Vreal,Vimag)
#	nstate=dssedata1.nstate
#	dssedata1.nstate = dssedata1.nstate + 2*griddata.nnode+2*griddata.nline
#	dssedata1.numnr = iteration
	dssedata1=dssedata()#we write the DSSE results in a dedicated class
	dssedata1.nstate=2*griddata.nnode+2*griddata.nline
	dssedata1.numnr = iteration
	nstate=0

	for x in range(griddata.nnode):
		Imagnload[x,0] = abs(Iloadreal[x,0]+1j*Iloadimag[x,0])
		Iphaseload[x,0] = cmath.phase(Iloadreal[x,0]+1j*Iloadimag[x,0])
		dssedata1.adddsseinstance(nstate,1,1,x,x,uncertaintytateV[2*x],Vmagnstatus[x,0]) #index represents the itemid for both nodes and voltages, both of them starts from zero
		nstate=nstate+1
		dssedata1.adddsseinstance(nstate,3,1,x,x,uncertaintytateV[2*x+1],Vphasestatus[x,0]) #index represents the itemid for both nodes and voltages, both of them starts from zero
		nstate=nstate+1
		#print("max err v.magnitude, bus "+str(x)+": "+str(300*uncertaintytateV[2*x])+" %")
		#print("max err v.ph.angle, bus "+str(x)+": "+str(300*uncertaintytateV[2*x+1])+" crad")
	for x in range(griddata.nline):
		Imagn[x,0] = abs(Ibranchireal[x,0]+1j*Ibranchiimag[x,0])
		Iphase[x,0] = cmath.phase(Ibranchireal[x,0]+1j*Ibranchiimag[x,0])
	#G1inv = numpy.linalg.inv(G2) # this is covariance matrix of the estimated state in rectangular format
	#G1inv = numpy.delete(G1inv, 0, 0)
	#G1inv = numpy.delete(G1inv, 0, 0)
	#G1inv = numpy.delete(G1inv, 0, 1)
	#G1inv = numpy.delete(G1inv, 0, 1)
	#var_mat=numpy.diag(G1inv); #variance output
	#uncertaintytate_rect=numpy.sqrt(var_mat);
	#print('uncertainty rect',uncertaintytate_rect*(4000000/(math.sqrt(3)*8660)))
	uncertaintytateI = calculatecovestimationI(G2,griddata,Imagn,Iphase,measdata)#we calculate the estimated uncertainty of the state
	#print('uncertainty I polar',uncertaintytateI*(4000000/(math.sqrt(3)*8660)))
	for x in range(griddata.nline):
		#print('line from',griddata.line[x].linefrom,'line to',griddata.line[x].lineto)
		dssedata1.adddsseinstance(nstate,5,1,griddata.line[x].linefrom,griddata.line[x].lineto,uncertaintytateI[2*x],Imagn[x,0]) #the uncertainty of the branch current is yet not calculated
		nstate=nstate+1
		dssedata1.adddsseinstance(nstate,7,1,griddata.line[x].linefrom,griddata.line[x].lineto,uncertaintytateI[2*x+1],Iphase[x,0])
		nstate=nstate+1
	# print("line current magnitude is")
	# print(Imagn)
	# print("line current phase angle is")
	# print(Iphase)
	dssedata1.numnr=iteration
	ttot1=datetime.now()-start1 #once we finish the estimation we calculate the required time
	dssedata1.time=ttot1.total_seconds()
	return dssedata1

def calccurrentsI(griddata,Vreal,Vimag): #function to calculate currents given two set of voltages
	Ibranchireal=numpy.zeros((griddata.nline,1));
	Ibranchiimag=numpy.zeros((griddata.nline,1));
	Ibranchfreal=numpy.zeros((griddata.nline,1));
	Ibranchfimag=numpy.zeros((griddata.nline,1));
	Ibranchf=numpy.zeros((griddata.nline,1));
	Iloadreal=numpy.zeros((griddata.nnode,1));
	Iloadimag=numpy.zeros((griddata.nnode,1));
	for x in range(griddata.nline):
		puinvbase=(griddata.pbase/((griddata.node[griddata.line[x].linefrom].vbase)**2))**(-1);
#		Ztemp = ((1/3) * (2*griddata.line[x].phaseP11[0].resistance + griddata.line[x].phaseP12[0].resistance  )) +1j*((1/3) * (2*griddata.line[x].phaseP11[0].reactance + griddata.line[x].phaseP12[0].reactance  ))
		Ztemp = griddata.line[x].phaseP11[0].resistance +1j*griddata.line[x].phaseP11[0].reactance
		Rinv = (puinvbase * (Ztemp)**(-1)).real
		Xinv = (puinvbase * (Ztemp)**(-1)).imag
		Gd=griddata.line[x].phaseP11[0].conductance
		G0=griddata.line[x].phaseP12[0].conductance
		Bd=griddata.line[x].phaseP11[0].susceptance
		B0=griddata.line[x].phaseP12[0].susceptance
#		Yshunt=puinvbase*((1/3)*(2*(1/(Gd+1j*Bd)) + (1/(G0+1j*B0))))**(-1)
		Yshunt=(Gd+1j*Bd)*puinvbase
		Ibranchi =   0.5*(Yshunt)* (Vreal[griddata.line[x].linefrom,0]+1j*Vimag[griddata.line[x].linefrom,0]) + (Rinv + 1j*Xinv)*((Vreal[griddata.line[x].linefrom,0]+1j*Vimag[griddata.line[x].linefrom,0]) - (Vreal[griddata.line[x].lineto  ,0]+1j*Vimag[griddata.line[x].lineto  ,0]))
		Ibranchf = - 0.5*(Yshunt)* (Vreal[griddata.line[x].lineto  ,0]+1j*Vimag[griddata.line[x].lineto  ,0]) + (Rinv + 1j*Xinv)*((Vreal[griddata.line[x].linefrom,0]+1j*Vimag[griddata.line[x].linefrom,0]) - (Vreal[griddata.line[x].lineto  ,0]+1j*Vimag[griddata.line[x].lineto  ,0]))
#		B	= puinvbase *  ((1/3) * (2*griddata.line[x].phaseP11[0].susceptance + griddata.line[x].phaseP12[0].susceptance  ))
#		G	= puinvbase *  ((1/3) * (2*griddata.line[x].phaseP11[0].conductance + griddata.line[x].phaseP12[0].conductance  ))
#		Ibranchi =   0.5*(G + 1j*B)* (Vreal[griddata.line[x].linefrom,0]+1j*Vimag[griddata.line[x].linefrom,0]) + (Rinv + 1j*Xinv)*((Vreal[griddata.line[x].linefrom,0]+1j*Vimag[griddata.line[x].linefrom,0]) - (Vreal[griddata.line[x].lineto  ,0]+1j*Vimag[griddata.line[x].lineto  ,0]))
#		Ibranchf = - 0.5*(G + 1j*B)* (Vreal[griddata.line[x].lineto  ,0]+1j*Vimag[griddata.line[x].lineto  ,0]) + (Rinv + 1j*Xinv)*((Vreal[griddata.line[x].linefrom,0]+1j*Vimag[griddata.line[x].linefrom,0]) - (Vreal[griddata.line[x].lineto  ,0]+1j*Vimag[griddata.line[x].lineto  ,0]))
		Ibranchireal[x,0]=Ibranchi.real;
		Ibranchiimag[x,0]=Ibranchi.imag;
		Ibranchfreal[x,0]=Ibranchf.real;
		Ibranchfimag[x,0]=Ibranchf.imag;
		Iloadreal[griddata.line[x].linefrom,0] = Iloadreal[griddata.line[x].linefrom,0] -  Ibranchireal[x,0]
		Iloadreal[griddata.line[x].lineto,0] = Iloadreal[griddata.line[x].lineto,0]   +  Ibranchfreal[x,0]
		Iloadimag[griddata.line[x].linefrom,0] = Iloadimag[griddata.line[x].linefrom,0] -  Ibranchiimag[x,0]
		Iloadimag[griddata.line[x].lineto,0] = Iloadimag[griddata.line[x].lineto,0]   +  Ibranchfimag[x,0]
	return Ibranchireal,Ibranchiimag,Ibranchfreal,Ibranchfimag,Iloadreal,Iloadimag

def hxvector1(griddata,measdata,Vreal,Vimag):#function to calculate the h(x) vector based on the current Netwon Raspon iteration status of voltages
	hx = numpy.zeros((measdata.nmeas,1))
	(Ibranchireal,Ibranchiimag,Ibranchfreal,Ibranchfimag,Iloadreal,Iloadimag)=calccurrentsI(griddata,Vreal,Vimag)
	for x in range(measdata.nmeas):
		if measdata.measurementinstance[x].mtype == 1 or measdata.measurementinstance[x].mtype == 2: #voltage magnitude measurements converted to real part
			hx[x,0]=Vreal[measdata.measurementinstance[x].itemid,0]
		if measdata.measurementinstance[x].mtype == 3 or measdata.measurementinstance[x].mtype == 4: #voltage phase angle measurements converted to imag part
			hx[x,0]=Vimag[measdata.measurementinstance[x].itemid,0]
		if measdata.measurementinstance[x].mtype == 5 or measdata.measurementinstance[x].mtype == 6: #current magnitude measurements converted to real part
			hx[x,0]=Ibranchireal[measdata.measurementinstance[x].itemid,0];
		if measdata.measurementinstance[x].mtype == 7 or measdata.measurementinstance[x].mtype == 8: #current phase angle measurements converted to imag part
			hx[x,0]=Ibranchiimag[measdata.measurementinstance[x].itemid,0];
		if measdata.measurementinstance[x].mtype == 9 or measdata.measurementinstance[x].mtype == 10: #active power injection measurements converted to current real part
			hx[x,0]=Iloadreal[measdata.measurementinstance[x].itemid,0];
		if measdata.measurementinstance[x].mtype == 11 or measdata.measurementinstance[x].mtype == 12: #reactive power injection measurements converted to current imaginary part
			hx[x,0]=Iloadimag[measdata.measurementinstance[x].itemid,0];
		if measdata.measurementinstance[x].mtype == 13 or measdata.measurementinstance[x].mtype == 14: #active power flow measurements converted to current real part
			hx[x,0]=Ibranchireal[measdata.measurementinstance[x].itemid,0];
		if measdata.measurementinstance[x].mtype == 15 or measdata.measurementinstance[x].mtype == 16: #reactive power flow measurements converted to current imaginary part
			hx[x,0]=Ibranchiimag[measdata.measurementinstance[x].itemid,0];
	return hx

def measurementvector1 (griddata,measdata,Vreal,Vimag):#this function restructures the input measuremenets in the z vector
	z = numpy.zeros((measdata.nmeas,1))
	for x in range(measdata.nmeas):
		if measdata.measurementinstance[x].mtype == 1 or measdata.measurementinstance[x].mtype == 2: #voltage magnitude measurements converted to real part
			for y in range(measdata.nmeas):
				if measdata.measurementinstance[y].itemid == measdata.measurementinstance[x].itemid and (measdata.measurementinstance[y].mtype == 3 or measdata.measurementinstance[y].mtype == 4) : #and measdata.measurementinstance[y].measurementaccuracy == measdata.measurementinstance[x].measurementaccuracy:
					ph1=measdata.measurementinstance[y].measurementvalue;
					temp=measdata.measurementinstance[x].measurementvalue*cmath.exp(1j*ph1)
					z[x,0]=temp.real;
		if measdata.measurementinstance[x].mtype == 3 or measdata.measurementinstance[x].mtype == 4: #voltage phase angle measurements converted to imag part
			for y in range(measdata.nmeas):
				if (measdata.measurementinstance[y].itemid == measdata.measurementinstance[x].itemid) and (measdata.measurementinstance[y].mtype == 1 or measdata.measurementinstance[y].mtype == 2) :  #and measdata.measurementinstance[y].measurementaccuracy == measdata.measurementinstance[x].measurementaccuracy:
					mag1=measdata.measurementinstance[y].measurementvalue;
					temp=mag1*cmath.exp(1j*measdata.measurementinstance[x].measurementvalue)
					z[x,0]=temp.imag;
		if measdata.measurementinstance[x].mtype == 5 or measdata.measurementinstance[x].mtype == 6: #current magnitude measurements converted to real part
			for y in range(measdata.nmeas):
				if (measdata.measurementinstance[y].itemid == measdata.measurementinstance[x].itemid) and (measdata.measurementinstance[y].mtype == 7 or measdata.measurementinstance[y].mtype == 8) :  #and measdata.measurementinstance[y].measurementaccuracy == measdata.measurementinstance[x].measurementaccuracy:
					ph1=measdata.measurementinstance[y].measurementvalue;
					temp=measdata.measurementinstance[x].measurementvalue*cmath.exp(1j*ph1)
					z[x,0]=temp.real;
		if measdata.measurementinstance[x].mtype == 7 or measdata.measurementinstance[x].mtype == 8: #current phase angle measurements converted to imag part
			for y in range(measdata.nmeas):
				if (measdata.measurementinstance[y].itemid == measdata.measurementinstance[x].itemid) and (measdata.measurementinstance[y].mtype == 5 or measdata.measurementinstance[y].mtype == 6) :  #and measdata.measurementinstance[y].measurementaccuracy == measdata.measurementinstance[x].measurementaccuracy:
					mag1=measdata.measurementinstance[y].measurementvalue;
					temp=mag1*cmath.exp(1j*measdata.measurementinstance[x].measurementvalue)
					z[x,0]=temp.imag;
		if measdata.measurementinstance[x].mtype == 9 or measdata.measurementinstance[x].mtype == 10: #active power injection
			for y in range(measdata.nmeas):
				if (measdata.measurementinstance[y].itemid == measdata.measurementinstance[x].itemid) and (measdata.measurementinstance[y].mtype == 11 or measdata.measurementinstance[y].mtype == 12):
					Q1=measdata.measurementinstance[y].measurementvalue;
					temp=+(measdata.measurementinstance[x].measurementvalue + 1j*Q1) / (Vreal[measdata.measurementinstance[x].itemid,0] + 1j*Vimag[measdata.measurementinstance[x].itemid,0])
					z[x,0]=temp.real;
		if measdata.measurementinstance[x].mtype == 11 or measdata.measurementinstance[x].mtype == 12: #reactive power injection
			for y in range(measdata.nmeas):
				if (measdata.measurementinstance[y].itemid == measdata.measurementinstance[x].itemid) and (measdata.measurementinstance[y].mtype == 9 or measdata.measurementinstance[y].mtype == 10):
					P1=measdata.measurementinstance[y].measurementvalue;
					temp=(P1 + 1j*measdata.measurementinstance[x].measurementvalue) / (Vreal[measdata.measurementinstance[x].itemid,0] + 1j*Vimag[measdata.measurementinstance[x].itemid,0])
					z[x,0]=-temp.imag;# the current is taken as complex conjugate
		if measdata.measurementinstance[x].mtype == 13 or measdata.measurementinstance[x].mtype == 14: #active power flow
			in1 = griddata.line[measdata.measurementinstance[x].itemid].linefrom #the voltage is the one at the beginning of the line
			for y in range(measdata.nmeas):
				if (measdata.measurementinstance[y].itemid == measdata.measurementinstance[x].itemid) and (measdata.measurementinstance[y].mtype == 15 or measdata.measurementinstance[y].mtype == 16):
					Q1=measdata.measurementinstance[y].measurementvalue;
					temp= (measdata.measurementinstance[x].measurementvalue + 1j*Q1) / (Vreal[in1,0] + 1j*Vimag[in1,0])
					z[x,0]=temp.real;
		if measdata.measurementinstance[x].mtype == 15 or measdata.measurementinstance[x].mtype == 16: #reactive power flow
			in1 = griddata.line[measdata.measurementinstance[x].itemid].linefrom #the voltage is the one at the beginning of the line
			for y in range(measdata.nmeas):
				if (measdata.measurementinstance[y].itemid == measdata.measurementinstance[x].itemid) and (measdata.measurementinstance[y].mtype == 13 or measdata.measurementinstance[y].mtype == 14):
					P1=measdata.measurementinstance[y].measurementvalue;
					temp=(P1 + 1j*measdata.measurementinstance[x].measurementvalue) / (Vreal[in1,0] + 1j*Vimag[in1,0])
					z[x,0]=-temp.imag;
	return z

def jacobianmatrixIRI1(griddata,measdata):#function to calculate the Jacobian of the system with current state
	# in the single phase case we take only phase A. Each element of phase A, is equal to 1/3*(2*pos+zero)
	R = numpy.zeros((1,griddata.nline)) #in the jacobian we always use the inverse of the series impedance
	X = numpy.zeros((1,griddata.nline))
	B = numpy.zeros((1,griddata.nline))
	G = numpy.zeros((1,griddata.nline))
	A = numpy.zeros((griddata.nline,griddata.nnode))
	jmatrix = numpy.zeros((measdata.nmeas,2*(griddata.nnode)))
	for m in range(griddata.nline): #logic matrix M lines X N nodes with A(m,n) = 1 if the node n is supplied by the line m
		for n in range(griddata.nnode):
			if griddata.line[m].lineto == n :
				A[m,n] = 1;
			elif griddata.line[m].linefrom == n :
				for t in range(griddata.nline):
					if A[t,griddata.line[m].linefrom] == 1 :
						A[t,griddata.line[m].lineto] = 1;
	for x in range(griddata.nline):
		puinvbase=(griddata.pbase/((griddata.node[0].vbase)**2))**(-1);
		pubase=(griddata.pbase/((griddata.node[0].vbase)**2));
		Ztemp= griddata.line[x].phaseP11[0].resistance +1j*griddata.line[x].phaseP11[0].reactance
		R[0,x] = (pubase*(Ztemp)).real
		X[0,x] = (pubase*(Ztemp)).imag

	for n in range(measdata.nmeas):
		if measdata.measurementinstance[n].mtype == 1 or measdata.measurementinstance[n].mtype == 2: # voltage magnitude measurement
			for index,row in enumerate(griddata.line): #we need to find the nodes connected
				if A[index,measdata.measurementinstance[n].itemid] == 1:
					jmatrix[n,2+2*index] = - R[0,index] #itemid refer to the node where the measurement is done
					jmatrix[n,2+2*index+1] = + X[0,index] #itemid refer to the node where the measurement is done
			jmatrix[n,0]  = 1;

		if measdata.measurementinstance[n].mtype == 3 or measdata.measurementinstance[n].mtype == 4: # voltage phase angle measurement
			for index,row in enumerate(griddata.line): #we need to find the nodes connected
				if A[index,measdata.measurementinstance[n].itemid] == 1:
					jmatrix[n,2+2*index]  = - X[0,index] #itemid refer to the node where the measurement is done
					jmatrix[n,2+2*index+1]  = - R[0,index] #itemid refer to the node where the measurement is done
			jmatrix[n,1]  = 1
			measdata.switchphmeas=1;

		if measdata.measurementinstance[n].mtype == 5 or measdata.measurementinstance[n].mtype == 6 or measdata.measurementinstance[n].mtype == 13 or measdata.measurementinstance[n].mtype == 14:
			#current magnitude measurement (converted to real) or active power flow measurement
			jmatrix[n,2*(measdata.measurementinstance[n].itemid)]=1;
		if measdata.measurementinstance[n].mtype == 7 or measdata.measurementinstance[n].mtype == 8 or measdata.measurementinstance[n].mtype == 15 or measdata.measurementinstance[n].mtype == 16:
			#current phase measurement (converted to imaginary) or reactive power flow measurement
			jmatrix[n,2*(measdata.measurementinstance[n].itemid)+1]=1;
		if measdata.measurementinstance[n].mtype == 9 or measdata.measurementinstance[n].mtype == 10:
		# active power injection measurement
			for index,row in enumerate(griddata.line): #we need to find the nodes connected
				if measdata.measurementinstance[n].itemid == griddata.line[index].lineto:
					jmatrix[n,2+2*index]=1;
				if measdata.measurementinstance[n].itemid == griddata.line[index].linefrom: #in this case it is the final node
					jmatrix[n,2+2*index]=-1;

		if measdata.measurementinstance[n].mtype == 11 or measdata.measurementinstance[n].mtype == 12:
		# reactive power injection measurement
			for index,row in enumerate(griddata.line): #we need to find the nodes connected
				if measdata.measurementinstance[n].itemid == griddata.line[index].lineto:
					jmatrix[n,2+2*index+1]=1;
				if measdata.measurementinstance[n].itemid == griddata.line[index].linefrom:
					jmatrix[n,2+2*index+1]=-1;
	if measdata.switchphmeas==0:
		jmatrix=numpy.delete(jmatrix, 1, 1)
	return jmatrix, measdata

def jacobianmatrixVRI1(griddata,measdata):#function to calculate the Jacobian of the system
	# in the single phase case we take only phase A. Each element of phase A, is equal to 1/3*(2*pos+zero)
	Rinv = numpy.zeros((1,griddata.nline)) #in the jacobian we always use the inverse of the series impedance
	Xinv = numpy.zeros((1,griddata.nline))
	B	= numpy.zeros((1,griddata.nline))
	G	= numpy.zeros((1,griddata.nline))
	jmatrix = numpy.zeros((measdata.nmeas,2*(griddata.nnode)))
	for x in range(griddata.nline):
		puinvbase=(griddata.pbase/((griddata.node[0].vbase)**2))**(-1);
		Ztemp= griddata.line[x].phaseP11[0].resistance +1j*griddata.line[x].phaseP11[0].reactance
		Rinv[0,x] = (puinvbase * (Ztemp)**(-1)).real
		Xinv[0,x] = (puinvbase * (Ztemp)**(-1)).imag
		B[0,x]	= puinvbase * griddata.line[x].phaseP11[0].susceptance
		G[0,x]	= puinvbase * griddata.line[x].phaseP11[0].conductance
	for n in range(measdata.nmeas):
		if measdata.measurementinstance[n].mtype == 1 or measdata.measurementinstance[n].mtype == 2: # voltage magnitude measurement
			jmatrix[n,2*(measdata.measurementinstance[n].itemid)]= 1 #itemid refer to the node where the measurement is done

		if measdata.measurementinstance[n].mtype == 3 or measdata.measurementinstance[n].mtype == 4: # voltage phase angle measurement
			jmatrix[n,2*(measdata.measurementinstance[n].itemid)+1]= 1; #itemid refer to the node where the measurement is done
			measdata.switchphmeas=1;

		if measdata.measurementinstance[n].mtype == 5 or measdata.measurementinstance[n].mtype == 6 or measdata.measurementinstance[n].mtype == 13 or measdata.measurementinstance[n].mtype == 14: # current magnitude measurement (converted to real) or active power flow measurement
			in1 = griddata.line[measdata.measurementinstance[n].itemid].linefrom
			#dI_re_1/dV_re_i_1
			jmatrix[n,2*(in1)]=0.5*G[0,measdata.measurementinstance[n].itemid]+Rinv[0,measdata.measurementinstance[n].itemid];
			#dI_re_1/dV_im_i_1
			jmatrix[n,2*(in1)+1]=-0.5*B[0,measdata.measurementinstance[n].itemid]-Xinv[0,measdata.measurementinstance[n].itemid];
		#if measdata.measurementinstance[n].itemid == griddata.line[index].lineto :
			fin1= griddata.line[measdata.measurementinstance[n].itemid].lineto
			#dI_re_1/dV_re_j_1
			jmatrix[n,2*(fin1)]=-0.5*G[0,measdata.measurementinstance[n].itemid]-Rinv[0,measdata.measurementinstance[n].itemid];
			#dI_re_1/dV_im_j_1
			jmatrix[n,2*(fin1)+1] =0.5*B[0,measdata.measurementinstance[n].itemid]+Xinv[0,measdata.measurementinstance[n].itemid];
		if measdata.measurementinstance[n].mtype == 7 or measdata.measurementinstance[n].mtype == 8 or measdata.measurementinstance[n].mtype == 15 or measdata.measurementinstance[n].mtype == 16:  # current phase measurement (converted to imaginary) or reactive power flow measurement
		#for index,row in enumerate(griddata.line): #we need to find initial and final node of that line
		#if measdata.measurementinstance[n].itemid == griddata.line[index].linefrom :
			in1 = griddata.line[measdata.measurementinstance[n].itemid].linefrom
			#dI_im_1/dV_re_i_1
			jmatrix[n,2*(in1)]=+0.5*B[0,measdata.measurementinstance[n].itemid]+Xinv[0,measdata.measurementinstance[n].itemid];
			#dI_im_1/dV_im_i_1
			jmatrix[n,2*(in1)+1]=+0.5*G[0,measdata.measurementinstance[n].itemid]+Rinv[0,measdata.measurementinstance[n].itemid];
		#if measdata.measurementinstance[n].itemid == griddata.line[index].lineto :
			fin1 = griddata.line[measdata.measurementinstance[n].itemid].lineto
			#dI_im_1/dV_re_j_1
			jmatrix[n,2*(fin1)]=-0.5*B[0,measdata.measurementinstance[n].itemid]-Xinv[0,measdata.measurementinstance[n].itemid];
			#dI_im_1/measdata.measurementinstance[n].itemid
			jmatrix[n,2*(fin1)+1]=-0.5*G[0,measdata.measurementinstance[n].itemid]-Rinv[0,measdata.measurementinstance[n].itemid];
		if measdata.measurementinstance[n].mtype == 9 or measdata.measurementinstance[n].mtype == 10: # active power injection measurement
			for index,row in enumerate(griddata.line): #we need to find the nodes connected
				if measdata.measurementinstance[n].itemid == griddata.line[index].linefrom:
					#dI_re_1/dV_re_i_1
					jmatrix[n,2*(measdata.measurementinstance[n].itemid)]=jmatrix[n,2*(measdata.measurementinstance[n].itemid)]-0.5*G[0,index]-Rinv[0,index];
					#dI_re_1/dV_im_i_1
					jmatrix[n,2*(measdata.measurementinstance[n].itemid)+1]=jmatrix[n,2*(measdata.measurementinstance[n].itemid)+1]+0.5*B[0,index]+Xinv[0,index];
					#dI_re_1/dV_re_j_1
					jmatrix[n,2*(griddata.line[index].lineto)]=+Rinv[0,index];
					#dI_re_1/dV_im_j_1
					jmatrix[n,2*(griddata.line[index].lineto)+1]  =-Xinv[0,index];
				if measdata.measurementinstance[n].itemid == griddata.line[index].lineto: #in this case it is the final node
					#dI_re_1/dV_re_i_1
					jmatrix[n,2*(measdata.measurementinstance[n].itemid)]=jmatrix[n,2*(measdata.measurementinstance[n].itemid)]-0.5*G[0,index]-Rinv[0,index];
					#dI_re_1/dV_im_i_1
					jmatrix[n,2*(measdata.measurementinstance[n].itemid)+1]=jmatrix[n,2*(measdata.measurementinstance[n].itemid)+1]+0.5*B[0,index]+Xinv[0,index];
					#dI_re_1/dV_re_j_1
					jmatrix[n,2*(griddata.line[index].linefrom)]=+Rinv[0,index];
					#dI_re_1/dV_im_j_1
					jmatrix[n,2*(griddata.line[index].linefrom)+1]  =-Xinv[0,index];

		if measdata.measurementinstance[n].mtype == 11 or measdata.measurementinstance[n].mtype == 12: # reactive power injection measurement
			for index,row in enumerate(griddata.line): #we need to find the nodes connected
				if measdata.measurementinstance[n].itemid == griddata.line[index].linefrom:
					#dI_re_1/dV_re_i_1
					jmatrix[n,2*(measdata.measurementinstance[n].itemid)]=jmatrix[n,2*(measdata.measurementinstance[n].itemid)]-0.5*B[0,index]-Xinv[0,index];
					#dI_re_1/dV_im_i_1
					jmatrix[n,2*(measdata.measurementinstance[n].itemid)+1]=jmatrix[n,2*(measdata.measurementinstance[n].itemid)+1] - 0.5*G[0,index] - Rinv[0,index];
					#dI_re_1/dV_re_j_1
					jmatrix[n,2*(griddata.line[index].lineto)]= + Xinv[0,index];
					#dI_re_1/dV_im_j_1
					jmatrix[n,2*(griddata.line[index].lineto)+1]  = + Rinv[0,index];
				if measdata.measurementinstance[n].itemid == griddata.line[index].lineto:
					#dI_re_1/dV_re_i_1
					jmatrix[n,2*(measdata.measurementinstance[n].itemid)]=jmatrix[n,2*(measdata.measurementinstance[n].itemid)]-0.5*B[0,index]-Xinv[0,index];
					#dI_re_1/dV_im_i_1
					jmatrix[n,2*(measdata.measurementinstance[n].itemid)+1]=jmatrix[n,2*(measdata.measurementinstance[n].itemid)+1] - 0.5*G[0,index] - Rinv[0,index];
					#dI_re_1/dV_re_j_1
					jmatrix[n,2*(griddata.line[index].linefrom)]= + Xinv[0,index];
					#dI_re_1/dV_im_j_1
					jmatrix[n,2*(griddata.line[index].linefrom)+1]  = + Rinv[0,index];
	if measdata.switchphmeas==0:
		jmatrix=numpy.delete(jmatrix, 1, 1)
	return jmatrix, measdata

def weightmatrixVRI1(griddata,measdata):#function to calculate the weight matrix of the measurement system
	CF=3 #coverage factor, to convert maximum errors into standard deviation
	wmatrix=numpy.zeros((measdata.nmeas,measdata.nmeas))
	for x in range(measdata.nmeas):
		if (measdata.measurementinstance[x].mtype > 8 and measdata.measurementinstance[x].mtype < 13): #active or reactive power injection measurements
			wmatrix[x,x] = ((measdata.measurementinstance[x].measurementaccuracy/CF) * measdata.measurementinstance[x].measurementvalue)**(-2);
		elif (measdata.measurementinstance[x].mtype > 12 and measdata.measurementinstance[x].mtype < 17): #active or reactive power flow measurements
			in1 = griddata.line[measdata.measurementinstance[x].itemid].linefrom
			wmatrix[x,x] = ((measdata.measurementinstance[x].measurementaccuracy/CF) * measdata.measurementinstance[x].measurementvalue)**(-2);
		elif measdata.measurementinstance[x].mtype < 5: #voltage magnitude and phase angle measurements
			wmatrix[x,x] = (measdata.measurementinstance[x].measurementaccuracy/CF)**(-2);
		elif measdata.measurementinstance[x].mtype == 5 or measdata.measurementinstance[x].mtype == 6: #current magnitude measurements
			for y in range(measdata.nmeas): #we look for the corresponding phase measurement, they need to be taken in the same itemid but the have different measurement types
				if (measdata.measurementinstance[y].itemid == measdata.measurementinstance[x].itemid) and (measdata.measurementinstance[y].mtype == 7 or measdata.measurementinstance[y].mtype == 8):
					Iphas=measdata.measurementinstance[y].measurementvalue;
					accphas=measdata.measurementinstance[y].measurementaccuracy;
					Imagn=measdata.measurementinstance[x].measurementvalue ;
					accmagn=measdata.measurementinstance[x].measurementaccuracy;
					wmatrix[x,x] = (((accmagn/CF)**2)*((numpy.cos(Iphas))**2) + ((accphas/CF)**2)*(Imagn**2)*((numpy.sin(Iphas))**2))**(-1)

		elif measdata.measurementinstance[x].mtype == 7 or measdata.measurementinstance[x].mtype == 8: #current phase measurements
			for y in range(measdata.nmeas): #we look for the corresponding phase measurement, they need to be taken in the same itemid but the have different measurement types
				if (measdata.measurementinstance[y].itemid == measdata.measurementinstance[x].itemid) and (measdata.measurementinstance[y].mtype == 5 or measdata.measurementinstance[y].mtype == 6):
					Iphas=measdata.measurementinstance[x].measurementvalue;
					accphas=measdata.measurementinstance[x].measurementaccuracy;
					Imagn=measdata.measurementinstance[y].measurementvalue ;
					accmagn=measdata.measurementinstance[y].measurementaccuracy;
					wmatrix[x,x] = (((accmagn/CF)**2)*((numpy.sin(Iphas))**2) + ((accphas/CF)**2)*(Imagn**2)*((numpy.cos(Iphas))**2))**(-1)
	return wmatrix



def calculatecovestimationV(G1,griddata,Vmagnstatus,Vphasestatus,measdata): #this function calculates the uncertainty of th estimated voltage
	G1inv = numpy.linalg.inv(G1) # this is covariance matrix of the estimated state in rectangular format
	if measdata.switchphmeas==0: #if there is meas and state at slack bus phase we add a zero column and row
		G1inv = numpy.vstack((G1inv[0,:],numpy.zeros((1,2*griddata.nnode-1)),G1inv[1:,:])); #we just need to do some transformations to obtain the one in polar format
		G1inv = numpy.column_stack((G1inv[:,0],numpy.zeros((2*(griddata.nnode),1)),G1inv[:,1:]));
	#var_err_rectangular=diag(G1inv)
	#std_err_rectangular=sqrt(var_err_rectangular)
	#rot11=numpy.eye(griddata.nnode)*(numpy.cos(Vphasestatus))#we slightly modify the inverse of the gain matrix, to obtain the uncertainty for magnitude and phase angles
	#rot12=numpy.eye(griddata.nnode)*(numpy.sin(Vphasestatus))
	#rot21 = numpy.eye(griddata.nnode)*(-numpy.sin(Vphasestatus)/Vmagnstatus)
	#rot22 = numpy.eye(griddata.nnode)*(numpy.cos(Vphasestatus)/Vmagnstatus)
	rotV = numpy.zeros((2*griddata.nnode,2*griddata.nnode));
	for x in range(griddata.nnode):
		rotV[2*x,2*x] = numpy.cos(Vphasestatus[x,0]);
		rotV[2*x,2*x+1] = numpy.sin(Vphasestatus[x,0]);
		rotV[2*x+1,2*x] = -numpy.sin(Vphasestatus[x,0])/Vmagnstatus[x,0];
		rotV[2*x+1,2*x] = -numpy.cos(Vphasestatus[x,0])/Vmagnstatus[x,0];
	#rotV = numpy.vstack((numpy.column_stack([rot11,rot12]), numpy.column_stack([rot21,rot22])))
	Rx_polar = rotV.dot(G1inv).dot(numpy.transpose(rotV));
	var_err_polar_mat=numpy.diag(Rx_polar); #variance output
	uncertaintytate=numpy.sqrt(var_err_polar_mat); #standard deviation (it is the uncertainty definition)
	return uncertaintytate

def calculatecovestimationI(G1,griddata,Imagnstatus,Iphasestatus,measdata): #this function calculates the uncertainty of th estimated voltage
	G1inv = numpy.linalg.inv(G1) # this is covariance matrix of the estimated state in rectangular format
	G1inv = numpy.delete(G1inv, 0, 0)
	G1inv = numpy.delete(G1inv, 0, 0)
	G1inv = numpy.delete(G1inv, 0, 1)
	G1inv = numpy.delete(G1inv, 0, 1)
	#if measdata.switchphmeas==0:
	#	G1inv = numpy.vstack((G1inv[0,:],numpy.zeros((1,2*griddata.nline-1)),G1inv[1:,:])); #we just need to do some transformations to obtain the one in polar format
	#	G1inv = numpy.column_stack((G1inv[:,0],numpy.zeros((2*(griddata.nline),1)),G1inv[:,1:]));
	rotI = numpy.zeros((2*griddata.nline,2*griddata.nline));
	for x in range(griddata.nline):
		rotI[2*x,2*x] = numpy.cos(Iphasestatus[x,0]);
		rotI[2*x,2*x+1] = numpy.sin(Iphasestatus[x,0]);
		rotI[2*x+1,2*x] = -numpy.sin(Iphasestatus[x,0])/Imagnstatus[x,0];
		rotI[2*x+1,2*x] = -numpy.cos(Iphasestatus[x,0])/Imagnstatus[x,0];
	#rot11 = numpy.eye(griddata.nline)*(numpy.cos(Iphasestatus))#we slightly modify the inverse of the gain matrix, to obtain the uncertainty for magnitude and phase angles
	#rot12 = numpy.eye(griddata.nline)*(numpy.sin(Iphasestatus))
	#rot21 = numpy.eye(griddata.nline)*(-numpy.sin(Iphasestatus)/Imagnstatus)
	#rot22 = numpy.eye(griddata.nline)*(numpy.cos(Iphasestatus)/Imagnstatus)
	#rotI = numpy.vstack((numpy.column_stack([rot11,rot12]), numpy.column_stack([rot21,rot22])))
	Rx_polar = rotI.dot(G1inv).dot(numpy.transpose(rotI));
	var_err_polar_mat=numpy.diag(Rx_polar); #variance output
	uncertaintytate=numpy.sqrt(var_err_polar_mat); #standard deviation (it is the uncertainty definition)
	return uncertaintytate


def connDB(databaseconn1): #function to connect to DB
	# print("DB name is '" + databaseconn1.DBname +"'\n")
	# print("user is '" + databaseconn1.user +"'\n")
	# print("password is '" + databaseconn1.password +"'\n")
	# print("ipaddress is '" + databaseconn1.ipaddress +"'\n")
	# print("port is is '" + databaseconn1.port +"'\n")
	connectionstring="dbname='" + databaseconn1.DBname +"' user='" + databaseconn1.user +"' host='" + databaseconn1.ipaddress +"' password='" + databaseconn1.password +"'";
	try:
		#DB connection parameters, change those if needed
		conn = psycopg2.connect(connectionstring)
	except psycopg2.OperationalError as e:
		print('Unable to connect!\n{0}').format(e)
		sys.exit(1)
	else:
		print('Connected to DB!')
	return conn


def readGridData(conn,groupgrid): #function to read grid data and rearrange in griddata class
	cur = conn.cursor()
	griddata1 = griddata() #pbase is 100 kW and gridtype
	griddata1.pbase = 100000;
	griddata1.vbasenom = 230;
	griddata1.gtype = groupgrid;
	SQLtext="";
	SQLtext += "SELECT id,vbase FROM networktopology.nodes WHERE groupnode = '"+str(groupgrid)+"' \n ORDER BY id ASC"
	try:
		cur.execute(SQLtext)
		conn.commit()
	except psycopg2.OperationalError as e:
		print('Unable to execute query!\n{0}').format(e)
		print('some issues when reading node data from DB')
		sys.exit(1)
	finally :
		print('successfully read node data')
		rows = cur.fetchall()
		print(SQLtext)
		for index, row in enumerate(rows):
			griddata1.addnode(row[0],float(row[1]))
		nnodes=index
	SQLtext="";
	SQLtext += "SELECT line_id,node1,node2,r_pos_m,x_pos_m,b_pos_m,g_pos_m,length,r_zero_m,x_zero_m,b_zero_m,g_zero_m,amax "
	SQLtext += "FROM networktopology.ltype INNER JOIN networktopology.lines ON networktopology.ltype.l_code = networktopology.lines.l_code "
	SQLtext += "WHERE groupline = '"+str(groupgrid)+"' ORDER BY line_id ASC\n"
	try:
		cur.execute(SQLtext)
		conn.commit()
	except psycopg2.OperationalError as e:
		print('Unable to execute query!\n{0}').format(e)
		print('some issues when reading node data from DB')
		sys.exit(1)
	finally :
		print('successfully read line data')
		rows = cur.fetchall()
		for index, row in enumerate(rows):
			griddata1.addline(row[0],1,1,1,row[1],row[2],float(row[12])) #row[0] is the itemid, row[1] is the initial node and row[2] the final node
			griddata1.line[index].addphaseP11(float(row[3])*float(row[7]),float(row[4])*float(row[7]),float(row[5])*float(row[7]),float(row[6])*float(row[7]))
			griddata1.line[index].addphaseP12(float(row[8])*float(row[7]),float(row[9])*float(row[7]),float(row[10])*float(row[7]),float(row[11])*float(row[7]))
			#the impedances and admittances quantities are in ohm and siemens, they have already been multiplied by the length of the lines
		nlines=index
	SQLtext="";
	SQLtext += "SELECT switch_id,node1,node2,switch_tripped,status,amax "
	SQLtext += "FROM networktopology.stype INNER JOIN networktopology.switches ON networktopology.stype.s_code = networktopology.switches.s_code "
	SQLtext += "WHERE groupswitch= '"+str(groupgrid)+"' ORDER BY switch_id ASC\n"
	try:
		cur.execute(SQLtext)
		conn.commit()
	except psycopg2.OperationalError as e:
		print('Unable to execute query!\n{0}').format(e)
		print('some issues when reading node data from DB')
		sys.exit(1)
	finally :
		print('successfully read line data')
		rows = cur.fetchall()
		for index, row in enumerate(rows):
			griddata1.addswitch(row[0],row[1],row[2],row[3],row[4],row[5]) #row[0] is the itemid, row[1] is the initial node and row[2] the final node
		nswitch=index
		griddata1.nline=nlines+1
		griddata1.nnode=nnodes+1
		griddata1.nswitch=nswitch+1
	return griddata1

def readGridDataFLISR(conn,linedata): #function to read grid data and rearrange in griddata class
	cur = conn.cursor()
	griddata1 = griddata() #pbase is 100 kW and gridtype
	griddata1.pbase = 100000;
	griddata1.vbasenom = 230;
	SQLtext="";
	SQLtext += "SELECT id,vbase FROM networktopology.nodes \n ORDER BY id ASC"
	try:
		cur.execute(SQLtext)
		conn.commit()
	except psycopg2.OperationalError as e:
		print('Unable to execute query!\n{0}').format(e)
		print('some issues when reading node data from DB')
		sys.exit(1)
	finally :
		print('successfully read node data')
		rows = cur.fetchall()
		print(SQLtext)
		for index, row in enumerate(rows):
			griddata1.addnode(row[0],float(row[1]))
		nnodes=index
		for index, linedata in enumerate(linedata):
			griddata1.addline(linedata[0],1,1,1,linedata[1],linedata[2],float(linedata[12])) #row[0] is the itemid, row[1] is the initial node and row[2] the final node
			griddata1.line[index].addphaseP11(float(linedata[3])*float(linedata[7]),float(linedata[4])*float(linedata[7]),float(linedata[5])*float(linedata[7]),float(linedata[6])*float(linedata[7]))
			griddata1.line[index].addphaseP12(float(linedata[8])*float(linedata[7]),float(linedata[9])*float(linedata[7]),float(linedata[10])*float(linedata[7]),float(linedata[11])*float(linedata[7]))
		nlines=index
		griddata1.nline=nlines+1
		griddata1.nnode=nnodes+1
#	SQLtext="";
#	SQLtext += "SELECT switch_id,node1,node2,switch_tripped,status,amax "
#	SQLtext += "FROM networktopology.stype INNER JOIN networktopology.switches ON networktopology.stype.s_code = networktopology.switches.s_code "
#	SQLtext += "ORDER BY switch_id ASC\n"
#	try:
#		cur.execute(SQLtext)
#		conn.commit()
#	except psycopg2.OperationalError as e:
#		print('Unable to execute query!\n{0}').format(e)
#		print('some issues when reading node data from DB')
#		sys.exit(1)
#	finally :
#		print('successfully read line data')
#		rows = cur.fetchall()
#		for index, row in enumerate(rows):
#			griddata1.addswitch(row[0],row[1],row[2],row[3],row[4],row[5]) #row[0] is the itemid, row[1] is the initial node and row[2] the final node
#		nswitch=index

#		griddata1.nswitch=nswitch+1
	return griddata1

def updateGridData(conn,groupgrid,griddata1):
	griddata2 = griddata1;
	for x in range(griddata1.nswitch):
		if griddata2.switch[x].status == 1: #we need to merge the corresponding nodes
			R = float(1000) / (griddata1.switch[x].amax^2);
			griddata2.addline(griddata1.nline,1,1,1,griddata1.switch[x].nodefrom,griddata1.switch[x].nodeto,griddata1.switch[x].amax) #row[0] is the itemid, row[1] is the initial node and row[2] the final node
			griddata2.line[griddata1.nline].addphaseP11(R,0,0,0)
			griddata2.line[griddata1.nline].addphaseP12(R,0,0,0)
			griddata2.nline=griddata1.nline+1;
	return griddata2

def writeDB(conn,dssedata): #function to write into postgres DB
	cur = conn.cursor()
	start3=datetime.utcnow()
	SQLtext0 = ""
	for x in range(dssedata.nstate):
		SQLtext = ""
		SQLtext += "SELECT scl_measurement_id FROM measurecommand.physicaldevice NATURAL JOIN measurecommand.logicaldevice NATURAL JOIN measurecommand.logicalnode NATURAL JOIN measurecommand.dataobject NATURAL JOIN measurecommand.dataattribute NATURAL JOIN "
		if int(dssedata.dsseinstance[x].mtype) == 1 or int(dssedata.dsseinstance[x].mtype) == 3:
			SQLtext += " measurecommand.measurement NATURAL JOIN  bridge.cim_scl_measurement NATURAL JOIN  networktopology.cim_measurement NATURAL JOIN networktopology.nodes"
			SQLtext += " WHERE  measurement_type='"+str(dssedata.dsseinstance[x].mtype)+"' AND measurecommand.measurement.dattribute_id_value = measurecommand.dataattribute.dattribute_id "
			SQLtext += " AND ldevice_id ='1' AND phase='AN' AND node_id='"+str(dssedata.dsseinstance[x].itemid)+"'  ORDER BY networktopology.nodes.id ASC"
		else:
			SQLtext += " measurecommand.measurement NATURAL JOIN  bridge.cim_scl_measurement NATURAL JOIN  networktopology.cim_measurement NATURAL JOIN networktopology.lines"
			SQLtext += " WHERE  measurement_type='"+str(dssedata.dsseinstance[x].mtype)+"' AND measurecommand.measurement.dattribute_id_value = measurecommand.dataattribute.dattribute_id "
			SQLtext += " AND ldevice_id ='1' AND phase='AN' AND line_id='"+str(dssedata.dsseinstance[x].itemid)+"'  ORDER BY networktopology.lines.id ASC"
		cur.execute(SQLtext)
		conn.commit()
		rows = cur.fetchall()
		SQLtext0+="INSERT INTO measurecommand.measurement_historian (scl_measurement_id, timestamp,value,quality) "
		SQLtext1 =""
		for index,row in enumerate(rows):
			mid=str(row[0])
			SQLtext0+=" ,VALUES ("+ mid +",'"+str(datetime.utcnow())+"','"+str(dssedata.dsseinstance[x].estimationvalue)+"','1')"
			SQLtext1+="UPDATE measurecommand.measurement_accuracy SET accuracy = '"+str(dssedata.dsseinstance[x].estimationaccuracy)+"' WHERE  scl_measurement_id = "+ mid +"; "
	SQLtext0+=";\n"
	SQLtext0+="INSERT INTO management.flag(ftype_id,flag_timestamp,flag_message) "
	SQLtext0 += " VALUES((select ftype_id FROM management.flag_index WHERE ftype_name = 'SE_RR'),'"+str(datetime.utcnow())+"','DSSE FINISHED');\n"
	cur.execute(SQLtext0)
	conn.commit()
	cur.execute(SQLtext1)
	conn.commit()
	ttot3=datetime.utcnow()-start3
	return ttot3.total_seconds()

def initDSSE(conn): #initialization of state estimator tables in database
	cur = conn.cursor()
	SQLtext=""
	SQLtext += "SELECT parameter_value FROM management.algorithm_parameter WHERE algorithm_id='1'"
	SQLtext += " AND (parameter_name ='DSSE_T_REAL_TIME') \n"
	cur.execute(SQLtext)
	conn.commit()
	rows = cur.fetchall()
	setdsse1=setdsse()
	setdsse1.trealset=float(rows[0][0])
	setdsse1.treal=float(rows[0][0])
	SQLtext=""
	SQLtext += "SELECT parameter_value FROM management.algorithm_parameter WHERE algorithm_id='1'"
	SQLtext += " AND (parameter_name ='DSSE_X_MEAS_PAST') \n"
	cur.execute(SQLtext)
	conn.commit()
	rows = cur.fetchall()
	setdsse1.xmeaspast=float(rows[0][0])
	SQLtext=""
	SQLtext += "SELECT parameter_value FROM management.algorithm_parameter WHERE algorithm_id='1'"
	SQLtext += " AND (parameter_name ='DSSE_X_MEAS_MIN') \n"
	cur.execute(SQLtext)
	conn.commit()
	rows = cur.fetchall()
	setdsse1.xmeasmin=float(rows[0][0])
	return setdsse1

def selectSQL(mtype,time1,time2,groupgrid):  #selection of data from DB
	if int(mtype) < 5 or ( int(mtype) > 8 and int(mtype) < 13): # in this case the measure refer to a node
		SQLtext=""
		SQLtext += "SELECT distinct on (networktopology.nodes.id) networktopology.nodes.id,networktopology.cim_measurement.phase, measurecommand.measurement_historian.value, accuracy,measurement_type,node_id FROM \n"
		SQLtext += "measurecommand.physicaldevice NATURAL JOIN \n"
		SQLtext += "measurecommand.logicaldevice NATURAL JOIN \n"
		SQLtext += "measurecommand.logicalnode NATURAL JOIN \n"
		SQLtext += "measurecommand.dataobject NATURAL JOIN \n"
		SQLtext += "measurecommand.dataattribute NATURAL JOIN \n"
		SQLtext += "measurecommand.measurement NATURAL JOIN \n"
		SQLtext += "measurecommand.measurement_accuracy NATURAL JOIN \n"
		SQLtext += "measurecommand.measurement_historian NATURAL JOIN \n"
		SQLtext += "bridge.cim_scl_measurement NATURAL JOIN \n"
		SQLtext += "networktopology.cim_measurement NATURAL JOIN \n"
		SQLtext += "networktopology.nodes --for line measurements \n"
		SQLtext += "WHERE ldevice_id != '1' AND  ldevice_id != '2' \n"
		SQLtext += "AND measurement_type = '"+ mtype + "' \n"
		SQLtext += "AND networktopology.cim_measurement.phase = 'AN' \n"
		SQLtext += "AND dattribute_id = dattribute_id_value \n"
		SQLtext += "AND measurecommand.measurement_historian.timestamp > '" + time1 + "'\n"
		SQLtext += "AND measurecommand.measurement_historian.timestamp < '" + time2 + "'\n"
		SQLtext += "AND groupnode = '"+str(groupgrid)+"'\n"
		SQLtext += "ORDER BY networktopology.nodes.id ASC, measurecommand.measurement_historian.timestamp DESC \n"

	else: # in this case the measure refer to a line
		SQLtext=""
		SQLtext += "SELECT distinct on (networktopology.lines.id) networktopology.lines.id,networktopology.cim_measurement.phase, measurecommand.measurement_historian.value, accuracy,measurement_type,line_id FROM \n"
		SQLtext += "measurecommand.physicaldevice NATURAL JOIN \n"
		SQLtext += "measurecommand.logicaldevice NATURAL JOIN \n"
		SQLtext += "measurecommand.logicalnode NATURAL JOIN \n"
		SQLtext += "measurecommand.dataobject NATURAL JOIN \n"
		SQLtext += "measurecommand.dataattribute NATURAL JOIN \n"
		SQLtext += "measurecommand.measurement NATURAL JOIN \n"
		SQLtext += "measurecommand.measurement_accuracy NATURAL JOIN \n"
		SQLtext += "measurecommand.measurement_historian NATURAL JOIN \n"
		SQLtext += "bridge.cim_scl_measurement NATURAL JOIN \n"
		SQLtext += "networktopology.cim_measurement NATURAL JOIN \n"
		SQLtext += "networktopology.lines --for line measurements \n"
		SQLtext += "WHERE ldevice_id != '1' AND  ldevice_id != '2' \n"
		SQLtext += "AND measurement_type = '"+ mtype + "' \n"
		SQLtext += "AND networktopology.cim_measurement.phase = 'AN' \n"
		SQLtext += "AND dattribute_id = dattribute_id_value \n"
		SQLtext += "AND measurecommand.measurement_historian.timestamp > '" + time1 + "'\n"
		SQLtext += "AND measurecommand.measurement_historian.timestamp < '" + time2 + "'\n"
		SQLtext += "AND groupline = '"+str(groupgrid)+"'\n"
		SQLtext += "ORDER BY networktopology.lines.id ASC, measurecommand.measurement_historian.timestamp DESC \n"
	return SQLtext

def selectSQLFLISR(mtype,time1,time2):  #selection of data from DB
	if int(mtype) < 5 or ( int(mtype) > 8 and int(mtype) < 13): # in this case the measure refer to a node
		SQLtext=""
		SQLtext += "SELECT distinct on (networktopology.nodes.id) networktopology.nodes.id,networktopology.cim_measurement.phase, measurecommand.measurement_historian.value, accuracy,measurement_type,node_id FROM \n"
		SQLtext += "measurecommand.physicaldevice NATURAL JOIN \n"
		SQLtext += "measurecommand.logicaldevice NATURAL JOIN \n"
		SQLtext += "measurecommand.logicalnode NATURAL JOIN \n"
		SQLtext += "measurecommand.dataobject NATURAL JOIN \n"
		SQLtext += "measurecommand.dataattribute NATURAL JOIN \n"
		SQLtext += "measurecommand.measurement NATURAL JOIN \n"
		SQLtext += "measurecommand.measurement_accuracy NATURAL JOIN \n"
		SQLtext += "measurecommand.measurement_historian NATURAL JOIN \n"
		SQLtext += "bridge.cim_scl_measurement NATURAL JOIN \n"
		SQLtext += "networktopology.cim_measurement NATURAL JOIN \n"
		SQLtext += "networktopology.nodes --for line measurements \n"
		SQLtext += "WHERE ldevice_id != '1' AND  ldevice_id != '2' \n"
		SQLtext += "AND measurement_type = '"+ mtype + "' \n"
		SQLtext += "AND networktopology.cim_measurement.phase = 'AN' \n"
		SQLtext += "AND dattribute_id = dattribute_id_value \n"
		SQLtext += "AND measurecommand.measurement_historian.timestamp > '" + time1 + "'\n"
		SQLtext += "AND measurecommand.measurement_historian.timestamp < '" + time2 + "'\n"
		SQLtext += "ORDER BY networktopology.nodes.id ASC, measurecommand.measurement_historian.timestamp DESC \n"

	else: # in this case the measure refer to a line
		SQLtext=""
		SQLtext += "SELECT distinct on (networktopology.lines.id) networktopology.lines.id,networktopology.cim_measurement.phase, measurecommand.measurement_historian.value, accuracy,measurement_type,line_id FROM \n"
		SQLtext += "measurecommand.physicaldevice NATURAL JOIN \n"
		SQLtext += "measurecommand.logicaldevice NATURAL JOIN \n"
		SQLtext += "measurecommand.logicalnode NATURAL JOIN \n"
		SQLtext += "measurecommand.dataobject NATURAL JOIN \n"
		SQLtext += "measurecommand.dataattribute NATURAL JOIN \n"
		SQLtext += "measurecommand.measurement NATURAL JOIN \n"
		SQLtext += "measurecommand.measurement_accuracy NATURAL JOIN \n"
		SQLtext += "measurecommand.measurement_historian NATURAL JOIN \n"
		SQLtext += "bridge.cim_scl_measurement NATURAL JOIN \n"
		SQLtext += "networktopology.cim_measurement NATURAL JOIN \n"
		SQLtext += "networktopology.lines --for line measurements \n"
		SQLtext += "WHERE ldevice_id != '1' AND  ldevice_id != '2' \n"
		SQLtext += "AND measurement_type = '"+ mtype + "' \n"
		SQLtext += "AND networktopology.cim_measurement.phase = 'AN' \n"
		SQLtext += "AND dattribute_id = dattribute_id_value \n"
		SQLtext += "AND measurecommand.measurement_historian.timestamp > '" + time1 + "'\n"
		SQLtext += "AND measurecommand.measurement_historian.timestamp < '" + time2 + "'\n"
		SQLtext += "ORDER BY networktopology.lines.id ASC, measurecommand.measurement_historian.timestamp DESC \n"
	return SQLtext

class setdsse(object):
	def __init__(self,trealset=1,treal=1,xmeaspast=10,xmeasmin=2):
		self.trealset = trealset
		self.treal = treal
		self.xmeaspast = xmeaspast
		self.xmeasmin = xmeasmin

class measdata(object):
	def __init__(self,nmeas=0,time=0,switchphmeas=0):
		self.nmeas = nmeas
		self.time = time
		self.switchphmeas = switchphmeas
		self.measurementinstance = []
	def addmeasurementinstance(self,measurementid,mtype,phase,itemid,measurementaccuracy,measurementvalue):
		self.measurementinstance.append(measurementinstance(measurementid,mtype,phase,itemid,measurementaccuracy,measurementvalue))

class dssedata(object):
	def __init__(self,nstate=0,numnr = 20, time = 1):
		self.nstate = nstate
		self.numnr = numnr
		self.time = time
		self.dsseinstance = []
	def adddsseinstance(self,estimationid,mtype,phase,itemid1,itemid2,estimationaccuracy,estimationvalue):
		self.dsseinstance.append(dsseinstance(estimationid,mtype,phase,itemid1,itemid2,estimationaccuracy,estimationvalue))

class dsseinstance(object):
	def __init__(self,estimationid,mtype,phase,itemid1,itemid2,estimationaccuracy,estimationvalue):
		self.estimationid = estimationid
		self.mtype = mtype
		self.phase  = phase
		self.itemid1  = itemid1
		self.itemid2  = itemid2
		self.estimationaccuracy  = estimationaccuracy
		self.estimationvalue  = estimationvalue

class measurementinstance(object):
	def __init__(self,measurementid,mtype,phase,itemid,measurementaccuracy,measurementvalue):
		self.measurementid = measurementid
		self.mtype = mtype
		self.phase  = phase
		self.itemid  = itemid
		self.measurementaccuracy  = measurementaccuracy
		self.measurementvalue  = measurementvalue

class griddata(object):
	def __init__(self,pbase = 1000000, gtype = 1,nnode=0,nline=0,nswitch=0,vbasenom=8660):
		self.pbase = pbase
		self.vbasenom = vbasenom
		self.nline = nline
		self.nnode = nnode
		self.nswitch = nswitch
		self.gtype = gtype
		self.node  = []
		self.line  = []
	def addnode(self,nodeid,vbase):
		self.node.append(node(nodeid,vbase))
	def addline(self,lineid,phasea,phaseb,phasec,linefrom,lineto,amax):#(row[0],1,1,1,row[1],row[2],float(row[9]))
		self.line.append(line(lineid,phasea,phaseb,phasec,linefrom,lineto,amax))
	def addswitch(self,switchid,nodefrom,nodeto,switchtripped,status,amax):
		self.node.append(switch(switchid,nodefrom,nodeto,switchtripped,status,amax))

class node(object):
	def __init__(self,nodeid,vbase=8660):
		self.nodeid = nodeid
		self.vbase=vbase

class switch(object):
	def __init__(self,switchid,nodefrom,nodeto,switchtripped,status,amax):
		self.switchid=switchid
		self.nodefrom=nodefrom
		self.nodeto=nodeto
		self.switchtripped=switchtripped
		self.status=status
		self.amax=amax

class line(object):
	def __init__(self,lineid,phasea,phaseb,phasec,linefrom,lineto,amax):
		self.lineid = lineid
		self.phasea=phasea
		self.phaseb=phaseb
		self.phasec=phasec
		self.linefrom=linefrom
		self.lineto=lineto
		self.amax=amax
		self.phaseP11=[]
		self.phaseP12=[]
		self.phaseP13=[]
		self.phaseP22=[]
		self.phaseP23=[]
		self.phaseP33=[]
	def addphaseP11(self,resistance,reactance,susceptance,conductance):
		self.phaseP11.append(phaseP11(resistance,reactance,susceptance,conductance))
	def addphaseP12(self,resistance,reactance,susceptance,conductance):
		self.phaseP12.append(phaseP12(resistance,reactance,susceptance,conductance))
	def addphaseP13(self,resistance,reactance,susceptance,conductance):
		self.phaseP13.append(phaseP13(resistance,reactance,susceptance,conductance))
	def addphaseP22(self,resistance,reactance,susceptance,conductance):
		self.phaseP22.append(phaseP22(resistance,reactance,susceptance,conductance))
	def addphaseP23(self,resistance,reactance,susceptance,conductance):
		self.phaseP23.append(phaseP23(resistance,reactance,susceptance,conductance))
	def addphaseP33(self,resistance,reactance,susceptance,conductance):
		self.phaseP33.append(phaseP33(resistance,reactance,susceptance,conductance))

class phaseP11(object):
	def __init__(self,resistance,reactance,susceptance,conductance):
		self.resistance=resistance
		self.reactance=reactance
		self.susceptance=susceptance
		self.conductance=conductance

class phaseP12(object):
	def __init__(self,resistance,reactance,susceptance,conductance):
		self.resistance=resistance
		self.reactance=reactance
		self.susceptance=susceptance
		self.conductance=conductance

class phaseP13(object):
	def __init__(self,resistance,reactance,susceptance,conductance):
		self.resistance=resistance
		self.reactance=reactance
		self.susceptance=susceptance
		self.conductance=conductance

class phaseP22(object):
	def __init__(self,resistance,reactance,susceptance,conductance):
		self.resistance=resistance
		self.reactance=reactance
		self.susceptance=susceptance
		self.conductance=conductance

class phaseP23(object):
	def __init__(self,resistance,reactance,susceptance,conductance):
		self.resistance=resistance
		self.reactance=reactance
		self.susceptance=susceptance
		self.conductance=conductance

class phaseP33(object):
	def __init__(self,resistance,reactance,susceptance,conductance):
		self.resistance=resistance
		self.reactance=reactance
		self.susceptance=susceptance
		self.conductance=conductance


class databaseconn(object):
	def __init__(self,DBname,user,password,ipaddress,port):
		self.DBname = DBname
		self.user = user
		self.password = password
		self.ipaddress = ipaddress
		self.port = port



