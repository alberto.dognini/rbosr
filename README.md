# <img src="doc/pictures/rbosr.png" width=80 /> Rule-Based Optimized Service Restoration (RBOSR)

## Contents

This repository contains the following material:

- Python code [`RBO_Service_Restoration_offline.py`](https://git.rwth-aachen.de/acs/public/automation/rbosr/blob/master/RBO_Service_Restoration_offline.py) of rule-based optimization algorithm for service restoration (RBOSR).
- Python code [`DSSE_Functions_FLISR.py`](https://git.rwth-aachen.de/acs/public/automation/rbosr/blob/master/DSSE_Functions_FLISR.py) that includes the distribution system state estimator functions
- Excel file [`Networkdata_MV_FLISR_use_case_1.xlsx`](https://git.rwth-aachen.de/acs/public/automation/rbosr/blob/master/Networkdata_MV_FLISR_use_case_1.xlsx) that includes all the necessary grid data to run the service restoration process.
- PDF file [`Networkdata_MV_FLISR_use_case.pdf`](https://git.rwth-aachen.de/acs/public/automation/rbosr/blob/master/Networkdata_MV_FLISR_use_case.pdf) that summarizes the relevant grid data.

## Usage

Instructions to use the service restoration algorithm:

 1. The two Python codes and the excel file must be located in the same folder.
 2. Configure the excel sheet, providing information about grid topology, loads, switches and fault location (set the tripped circuit breakers).
 3. If changed, set the excel file name in the RBOSR Python code (in line 1726).
 4. Set the weighting factors `u1`, `u2`, `u3` and `u4` according to the desired restoration target in the RBOSR Python code (in line 1714). Additional information are found in the paper "Rule-Based Optimization Algorithm for Service Restoration of Active Distribution Grids", authors: A. Dognini, A. Sadu, A. Angioni, F. Ponci, A. Monti, currently under revision.
 5. Set the information about the PostgreSQL database in the RBOSR Python code (in line 1679)

Considerations about the RBOSR Python code:

 - The code has been developed with Python 3.6
 - It needs, among others, the installation of the following libraries:
   - `pandas`
   - `networkx`
   - `psycopg2`
 - It needs to interface a PostgreSQL database, from/to which the data are retrieved/published.
 - It generates an Excel sheets (line 1703) with information about the elapsed computation time.

 Run the following command to install the required dependencies:

```
pip install -r requirements.txt
```

## Documentation

Documentation about the distribution system state estimator can be found in the following website:
https://git.rwth-aachen.de/acs/public/automation/rbosr

## Publications

For additional information, refer to the following articles:

- Dognini, Alberto; Sadu, Abhinav; Angioni, Andrea; Ponci, Ferdinanda; Monti, Antonello; "Service Restoration Algorithm for Distribution Grids under High Impact Low Probability Events"; Smart grids: key enablers of a green power system : 2020 IEEE PES Innovative Smart Grid Technologies Europe : Delft, the Netherlands, 26-28 October, 2020 / IEEE PES - Power & Energy Society, Innovative Smart Grid Technologies - 2020 ISGT Europe, IEEE, TU Delft, 237-241

- C. Muscas, S. Sulis, A. Angioni, F. Ponci, and A. Monti, "Impact of Different Uncertainty Sources on a Three-Phase State Estimator for Distribution Networks", IEEE Transactions on Instrumentation and Measurement, vol. 63, no. 9, pp. 2200-2209, Sep. 2014.
- A. Angioni, A. Kulmala, D. D. Giustina, M. Mirz, A. Mutanen, A. Ded,F.  Ponci,  L.  Shengye,  G.  Massa,  S.  Repo,  and  A.  Monti,  "Design  andimplementation of a substation automation unit", IEEE Transactions on Power Delivery, vol. 32, no. 2, pp. 1133-1142, April 2017.
- P. Jamborsalamati, A. Sadu, F. Ponci, A. Monti and M. J. Hossain,"Improvement of supply  restoration in multi-spare-feeder active distri-bution grids using IEC 61850", in 2017 IEEE Innovative Smart Grid Technologies - Asia (ISGT-Asia), Dec 2017, pp. 1-5.

## Copyright

2019, Institute for Automation of Complex Power Systems, EONERC

## License

This project is released under the terms of the [GPL version 3](COPYING.md).

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

For other licensing options please consult [Prof. Antonello Monti](mailto:amonti@eonerc.rwth-aachen.de).

## Contact

[![EONERC ACS Logo](doc/pictures/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- Alberto Dognini <adognini@eonerc.rwth-aachen.de>
- Abhinav Sadu <asadu@eonerc.rwth-aachen.de>

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)
[EON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)
